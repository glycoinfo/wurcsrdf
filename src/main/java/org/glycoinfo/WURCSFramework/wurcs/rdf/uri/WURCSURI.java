package org.glycoinfo.WURCSFramework.wurcs.rdf.uri;


/**
 * Class for generation of URI for WURCS-RDF components
 * @author MasaakiMatsubara
 *
 */
public class WURCSURI extends GlycoinfoURI {

	// For URIs not contained accession number
	/** http://rdf.glycoinfo.org/glycan/wurcs/2.0/{WURCS} */
	public static String getWURCS ( String a_strWURCS ) { return concatURI(WURCSBaseURI.WURCS, a_strWURCS); };
	/** http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide/{MS(ResidueCode)} */
	public static String getMS    ( String a_strMS )    { return concatURI(WURCSBaseURI.MS,   a_strMS);     };
	/** http://rdf.glycoinfo.org/glycan/wurcs/2.0/SkeletonCode/{SkeletonCode} */
	public static String getSC    ( String a_strSC )    { return concatURI(WURCSBaseURI.SC,   a_strSC);     };

	/** http://rdf.glycoinfo.org/glycan/wurcs/2.0/MOD/{MOD} */
	public static String getMOD   ( String a_strMOD )   { return concatURI(WURCSBaseURI.MOD,  a_strMOD);    };
	/** http://rdf.glycoinfo.org/glycan/wurcs/2.0/MAP/{MAP} */
	public static String getMAP   ( String a_strMAP )   { return concatURI(WURCSBaseURI.MAP,  a_strMAP);    };
	/** http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIPS/{LIPS} */
	public static String getLIPS  ( String a_strLIPS )  { return concatURI(WURCSBaseURI.LIPS, a_strLIPS);   };
	/** http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIP/{LIP} */
	public static String getLIP   ( String a_strLIP )   { return concatURI(WURCSBaseURI.LIP,  a_strLIP);    };

	// For URIs contained accession number
	/** http://rdf.glycoinfo.org/glycan/{AccNum}/wurcs/2.0/UniqueRES/{UniqueRES_ID} */
	public static String getURES  ( String a_strAccNum, Integer a_iURESID ) { return concatURI(WURCSBaseURI.URES(a_strAccNum),  a_iURESID);  };
	/** http://rdf.glycoinfo.org/glycan/{AccNum}/wurcs/2.0/RES/{RES_ID} */
	public static String getRES   ( String a_strAccNum, Integer a_iRESID )  { return concatURI(WURCSBaseURI.RES(a_strAccNum),   a_iRESID);   };
	/** http://rdf.glycoinfo.org/glycan/{AccNum}/wurcs/2.0/LIN/{LIN} */
	public static String getLIN   ( String a_strAccNum, String a_strLIN )   { return concatURI(WURCSBaseURI.LIN(a_strAccNum),   a_strLIN);   };
	/** http://rdf.glycoinfo.org/glycan/{AccNum}/wurcs/2.0/GLIPS/{GLIPS} */
	public static String getGLIPS ( String a_strAccNum, String a_strGLIPS ) { return concatURI(WURCSBaseURI.GLIPS(a_strAccNum), a_strGLIPS); };
	/** http://rdf.glycoinfo.org/glycan/{AccNum}/wurcs/2.0/GLIP/{GLIP} */
	public static String getGLIP  ( String a_strAccNum, String a_strGLIP )  { return concatURI(WURCSBaseURI.GLIP(a_strAccNum),  a_strGLIP);  };


}
