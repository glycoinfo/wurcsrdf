package org.glycoinfo.WURCSFramework.wurcs.rdf.uri;

/**
 * Class for base URI using WURCS RDF
 * @author MasaakiMatsubara
 *
 */
public class WURCSBaseURI extends BaseURI {

	private static final String WURCS_VER = "/wurcs/2.0";

	/* For WURCS component with no AccNum */
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0
	public static final String WURCS = GLYCAN+WURCS_VER;
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/Monosaccharide
	public static final String MS   = WURCS+"/Monosaccharide";

	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/Backbone
	public static final String BACK = WURCS+"/Backbone";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/SkeletonCode
	public static final String SC   = WURCS+"/SkeletonCode";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/BackboneCarbon
	public static final String BC   = WURCS+"/BackboneCarbon";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/CabonDescriptor // using instance in CarbonDescriptorURI
//	public static final String CD   = WURCS+"/CarbonDescriptor";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/Anomer
	public static final String ANOM = WURCS+"/Anomer";

	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/MOD
	public static final String MOD  = WURCS+"/MOD";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/MAP
	public static final String MAP  = WURCS+"/MAP";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIPS
	public static final String LIPS = WURCS+"/LIPS";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIP
	public static final String LIP  = WURCS+"/LIP";

	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/Repeat
	public static final String REP  = WURCS+"/Repeat";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/Probability
	public static final String PROB = WURCS+"/Probability";

	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/Repeat_count
	public static final String REP_CNT  = WURCS+"/Repeat_count";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/Probability_value
	public static final String PROB_VAL = WURCS+"/Probability_value";

	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/Carbon_number
	public static final String CNUM     = WURCS+"/Carbon_number";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/UniqueRES_number
	public static final String URES_NUM = WURCS+"/UniqueRES_number";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/RES_index
	public static final String RES_ID   = WURCS+"/RES_index";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/MAP_star_number
	public static final String MAP_NUM  = WURCS+"/MAP_star_number";

	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/Count_section
	public static final String CNT_SECT = WURCS+"/Count_section";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/UniqueRES_section
	public static final String URES_SECT = WURCS+"/UniqueRES_section";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/RES_sequence_section
	public static final String RES_SECT  = WURCS+"/RES_sequence_section";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIN_section
	public static final String LIN_SECT  = WURCS+"/LIN_section";

	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/UniqueRES
	public static final String URES = WURCS+"/UniqueRES";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/RES
	public static final String RES  = WURCS+"/RES";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/LIN
	public static final String LIN  = WURCS+"/LIN";

	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIPS
	public static final String GLIPS = WURCS+"/GLIPS";
	// http://rdf.glycoinfo.org/glycan/wurcs/2.0/GLIP
	public static final String GLIP  = WURCS+"/GLIP";

	// For WURCS component with AccNum
	/** http://rdf.glycoinfo.org/glycan/{AccNum}/wurcs/2.0 */
	public static final String WURCS ( String a_strAccNum ) { return GLYCAN(a_strAccNum)+WURCS_VER;   };
	/** http://rdf.glycoinfo.org/glycan/{AccNum}/wurcs/2.0/UniqueRES */
	public static final String URES  ( String a_strAccNum ) { return WURCS(a_strAccNum)+"/UniqueRES"; };
	/** http://rdf.glycoinfo.org/glycan/{AccNum}/wurcs/2.0/RES */
	public static final String RES   ( String a_strAccNum ) { return WURCS(a_strAccNum)+"/RES";       };
	/** http://rdf.glycoinfo.org/glycan/{AccNum}/wurcs/2.0/LIN */
	public static final String LIN   ( String a_strAccNum ) { return WURCS(a_strAccNum)+"/LIN";       };
	/** http://rdf.glycoinfo.org/glycan/{AccNum}/wurcs/2.0/GLIPS */
	public static final String GLIPS ( String a_strAccNum ) { return WURCS(a_strAccNum)+"/GLIPS";     };
	/** http://rdf.glycoinfo.org/glycan/{AccNum}/wurcs/2.0/GLIP */
	public static final String GLIP  ( String a_strAccNum ) { return WURCS(a_strAccNum)+"/GLIP";      };

}
