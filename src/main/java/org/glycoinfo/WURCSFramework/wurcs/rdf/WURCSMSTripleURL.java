package org.glycoinfo.WURCSFramework.wurcs.rdf;

import java.net.URL;

import org.glycoinfo.WURCSFramework.util.WURCSStringUtils;
import org.glycoinfo.WURCSFramework.wurcs.rdf.constant.GLYCAN_OLD;
import org.glycoinfo.WURCSFramework.wurcs.rdf.constant.WURCS_OLD;

public enum WURCSMSTripleURL {
	// For monosaccharide in WURCSSequence
	CORE_MS()	{ public String get(String ms, Object val)	{ return GLYCAN_OLD.BASE_URL+WURCS_OLD.VERSION+"/core/"+urlenc(ms) ;} },
	CORE_BRG()	{ public String get(String ms, Object val)	{ return GLYCAN_OLD.BASE_URL+WURCS_OLD.VERSION+"/core/"+urlenc(ms)+"/bridge/"+urlenc(objectToString(val)) ;} },
	CORE_SUBST(){ public String get(String ms, Object val)	{ return GLYCAN_OLD.BASE_URL+WURCS_OLD.VERSION+"/core/"+urlenc(ms)+"/substituent/"+urlenc(objectToString(val)) ;} },
	MS()		{ public String get(String ms, Object val)	{ return GLYCAN_OLD.BASE_URL+WURCS_OLD.VERSION+"/monosaccharide/"+urlenc(ms) ;}},
	BRG()		{ public String get(String ms, Object val)	{ return GLYCAN_OLD.BASE_URL+WURCS_OLD.VERSION+"/monosaccharide/"+urlenc(ms)+"/bridge/"+urlenc(objectToString(val)) ;} },
	SUBST()		{ public String get(String ms, Object val)	{ return GLYCAN_OLD.BASE_URL+WURCS_OLD.VERSION+"/monosaccharide/"+urlenc(ms)+"/substituent/"+urlenc(objectToString(val)) ;} },
	BASE_TYPE()	{ public String get(String ms, Object val)	{ return GLYCAN_OLD.BASE_URL+WURCS_OLD.VERSION+"/basetype/"+urlenc(objectToString(val)) ;}},
	ANO_BASE()	{ public String get(String ms, Object val)	{ return GLYCAN_OLD.BASE_URL+WURCS_OLD.VERSION+"/anobase/"+urlenc(objectToString(val)) ;}},
	SC()		{ public String get(String ms, Object val)	{ return GLYCAN_OLD.BASE_URL+WURCS_OLD.VERSION+"/SkeletonCode/"+urlenc(objectToString(val)) ;}};

	abstract public String get(String ms, Object value);

	private static String urlenc(String str){
		return  WURCSStringUtils.getURLString(str);
	}

	private static String objectToString(Object obj){
		if(obj instanceof  String){
			return (String)obj;
		}else if( obj instanceof Integer){
			return String.valueOf((Integer)obj);
		}else if( obj instanceof Double){
			return String.valueOf((Double)obj);
		}else if( obj instanceof Boolean){
			return String.valueOf((Boolean)obj);
		}else if( obj instanceof Character){
			return String.valueOf((Character)obj);
		}else if( obj instanceof URL){
			return ((URL)obj).toString();
		}
		return null;
	}

}
