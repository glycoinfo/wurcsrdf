package org.glycoinfo.WURCSFramework.wurcs.rdf;

import java.util.HashMap;

import org.glycoinfo.WURCSFramework.util.array.WURCSExporter;
import org.glycoinfo.WURCSFramework.util.array.analysis.WURCSCompositionCalculator;
import org.glycoinfo.WURCSFramework.wurcs.array.WURCSArray;
import org.glycoinfo.WURCSFramework.wurcs.owl.GLYCORDF;
import org.glycoinfo.WURCSFramework.wurcs.rdf.uri.BaseURI;
import org.glycoinfo.WURCSFramework.wurcs.rdf.uri.GlycoinfoURI;
import org.glycoinfo.WURCSFramework.wurcs.rdf.uri.WURCSBaseURI;
import org.glycoinfo.WURCSFramework.wurcs.rdf.uri.WURCSURI;
import org.openrdf.model.vocabulary.RDF;

public class WURCSRDFModelMSComponentAndAlias extends WURCSRDFModel {

	@SuppressWarnings("unused")
	private WURCSRDFModelMSComponentAndAlias(){}

	public WURCSRDFModelMSComponentAndAlias(String a_strAccessionNumber, WURCSArray a_oWURCS, boolean a_bDefault){
		super(a_bDefault);
		createTripleModel(a_strAccessionNumber, a_oWURCS);
	}

	public void addWURCSMSAliasTripleModel(String a_strAccessionNumber, WURCSArray a_oWURCS) {
		createTripleModel(a_strAccessionNumber, a_oWURCS);
	}

	private WURCSRDFModelMSComponentAndAlias createTripleModel(String a_strAccessionNumber, WURCSArray a_oWURCS){
		WURCSExporter export = new WURCSExporter();

		// # WURCS
		String t_strWURCSString = export.getWURCSString(a_oWURCS);

		WURCSRDFModelMSComponentAndAlias model = this;

		// Saccharide triple
		model.addTriple(createResourceTriple(
				BaseURI.GLYCAN(a_strAccessionNumber),
				RDF.TYPE.stringValue(),
				GLYCORDF.A_SACCHARIDE));

		model.addTriple(createResourceTriple(
				BaseURI.GLYCAN(a_strAccessionNumber),
				GLYCORDF.HAS_GSEQ,
				WURCSBaseURI.WURCS(a_strAccessionNumber) ));


		// WURCS is a Glycosequence
		model.addTriple(createResourceTriple(
				WURCSBaseURI.WURCS(a_strAccessionNumber),
				RDF.TYPE.stringValue(),
				GLYCORDF.A_GSEQ));

		// WURCS has sequence
		model.addTriple(createLiteralTriple(
				WURCSBaseURI.WURCS(a_strAccessionNumber),
				GLYCORDF.HAS_SEQ,
				t_strWURCSString));

		// For format WURCS
		model.addTriple(createResourceTriple(
				WURCSBaseURI.WURCS(a_strAccessionNumber),
				GLYCORDF.IN_CARB_FORMAT,
				GLYCORDF.FORMAT_WURCS));

		// For each component
		HashMap<String, Integer> t_mapMSToCardinality
			= (new WURCSCompositionCalculator()).getCardinalityMap(a_oWURCS);

		for ( String t_strMS : t_mapMSToCardinality.keySet() ) {
			int t_iCardinality = t_mapMSToCardinality.get(t_strMS);

			// Concatenate MS string and its cardinality
			// {cardinality}_{MS(WURCS)}
			String t_strComponentString = t_iCardinality+"_"+t_strMS;

			// Saccharide has component
			model.addTriple(createResourceTriple(
					BaseURI.GLYCAN(a_strAccessionNumber),
					GLYCORDF.HAS_COMPONENT,
					GlycoinfoURI.getComponent(t_strComponentString) ));

			// A compoment
			model.addTriple(createResourceTriple(
					GlycoinfoURI.getComponent(t_strComponentString),
					RDF.TYPE.stringValue(),
					GLYCORDF.A_COMPONENT ));

			// Component has cardinality (Integer)
			model.addTriple(createLiteralTriple(
					GlycoinfoURI.getComponent(t_strComponentString),
					GLYCORDF.HAS_CARDINALITY,
					t_iCardinality ));

			// Component has MS
			model.addTriple(createResourceTriple(
					GlycoinfoURI.getComponent(t_strComponentString),
					GLYCORDF.HAS_MS,
					WURCSURI.getMS(t_strMS) ));

			// Create MS_alias
			this.createMSAliasTripleModel(t_strMS);
		}

		return model;
	}

	private WURCSRDFModelMSComponentAndAlias createMSAliasTripleModel(String a_strMS) {
		WURCSRDFModelMSComponentAndAlias model = this;

		// MS is a monosaccharide
		model.addTriple(createResourceTriple(
				WURCSURI.getMS(a_strMS),
				RDF.TYPE.stringValue(),
				GLYCORDF.A_MS ));

		// MS has MS_alias
		model.addTriple(createResourceTriple(
				WURCSURI.getMS(a_strMS),
				GLYCORDF.HAS_ALIAS,
				GlycoinfoURI.getMSAliasWURCS(a_strMS) ));

		// MS_alias is a monosaccharide_alias
		model.addTriple(createResourceTriple(
				GlycoinfoURI.getMSAliasWURCS(a_strMS),
				RDF.TYPE.stringValue(),
				GLYCORDF.A_MS_ALIAS ));

		// MS_alias has alias_name (String)
		model.addTriple(createLiteralTriple(
				GlycoinfoURI.getMSAliasWURCS(a_strMS),
				GLYCORDF.HAS_ALIAS_NAME,
				a_strMS ));

		// Notation scheme is WURCS
		model.addTriple(createResourceTriple(
				GlycoinfoURI.getMSAliasWURCS(a_strMS),
				GLYCORDF.HAS_MS_SCHME,
				GLYCORDF.MS_SCHEME_WURCS ));

		return this;
	}
}
