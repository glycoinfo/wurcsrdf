package org.glycoinfo.WURCSFramework.wurcs.rdf.constant;

public class SUBSUMPTION {
//	private static Model m_model = ModelFactory.createDefaultModel();
	public static final String PREFIX = "relation";
	public static final String NS = "http://www.glycoinfo.org/glyco/owl/relation#";
	public static final String VERSION = "";
	public static String getPrefix() {return PREFIX;}
	public static String getURI() {return NS;}
	public static String getURI(String val) {return NS+val;}
//	public static final Resource NAMESPACE = m_model.createResource( NS );
//	public static final Resource FULL_LANG = m_model.getResource( getURI() );
	public static final String BASE_URL = GLYCAN_OLD.BASE_URL+"/"+VERSION;


}
