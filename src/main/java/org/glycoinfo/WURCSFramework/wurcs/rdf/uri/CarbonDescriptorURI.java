package org.glycoinfo.WURCSFramework.wurcs.rdf.uri;

import org.glycoinfo.WURCSFramework.wurcs.graph.CarbonDescriptor;
import org.glycoinfo.WURCSFramework.wurcs.owl.WURCSOWL;

public enum CarbonDescriptorURI implements WURCSIndividualURI {

	SZ3_METHYL_L   ( CarbonDescriptor.SZ3_METHYL_L   , "terminal_carbon_descriptor_m" ), // 'm': -C(H)(H)(H)
	SZ3_METHYL_U   ( CarbonDescriptor.SZ3_METHYL_U   , "terminal_carbon_descriptor_M" ), // 'M': -C(X)(X)(X)
	SZ3_HYDROXYL   ( CarbonDescriptor.SZ3_HYDROXYL   , "terminal_carbon_descriptor_h" ), // 'h': -C(H)(H)(X)
	SZ3_ACETAL_L   ( CarbonDescriptor.SZ3_ACETAL_L   , "terminal_carbon_descriptor_c" ), // 'c': -C(X)(X)(H)
	SZ3_ACETAL_U   ( CarbonDescriptor.SZ3_ACETAL_U   , "terminal_carbon_descriptor_C" ), // 'C': -C(X)(X)(H)
	SZ3_STEREO_S_L ( CarbonDescriptor.SZ3_STEREO_S_L , "terminal_carbon_descriptor_1" ), // '1': -C(X)(Y)(H)
	SZ3_STEREO_R_L ( CarbonDescriptor.SZ3_STEREO_R_L , "terminal_carbon_descriptor_2" ), // '2': -C(X)(Y)(H)
	SZ3_STEREO_s_L ( CarbonDescriptor.SZ3_STEREO_s_L , "terminal_carbon_descriptor_3" ), // '3': -C(X)(Y)(H)
	SZ3_STEREO_r_L ( CarbonDescriptor.SZ3_STEREO_r_L , "terminal_carbon_descriptor_4" ), // '4': -C(X)(Y)(H)
	SZ3_STEREO_X_L ( CarbonDescriptor.SZ3_STEREO_X_L , "terminal_carbon_descriptor_x" ), // 'x': -C(X)(Y)(H)
	SZ3_STEREO_S_U ( CarbonDescriptor.SZ3_STEREO_S_U , "terminal_carbon_descriptor_5" ), // '5': -C(X)(Y)(Z)
	SZ3_STEREO_R_U ( CarbonDescriptor.SZ3_STEREO_R_U , "terminal_carbon_descriptor_6" ), // '6': -C(X)(Y)(Z)
	SZ3_STEREO_s_U ( CarbonDescriptor.SZ3_STEREO_s_U , "terminal_carbon_descriptor_7" ), // '7': -C(X)(Y)(Z)
	SZ3_STEREO_r_U ( CarbonDescriptor.SZ3_STEREO_r_U , "terminal_carbon_descriptor_8" ), // '8': -C(X)(Y)(Z)
	SZ3_STEREO_X_U ( CarbonDescriptor.SZ3_STEREO_X_U , "terminal_carbon_descriptor_X" ), // 'X': -C(X)(Y)(Z)
	SZ2_ALDEHYDE   ( CarbonDescriptor.SZ2_ALDEHYDE   , "terminal_carbon_descriptor_o" ), // 'o': -C(=X)(H)
	SZ2_ACID_U     ( CarbonDescriptor.SZ2_ACID_U     , "terminal_carbon_descriptor_A" ), // 'A': -C(=X)(Y)
	DZ2_METHYLENE_L( CarbonDescriptor.DZ2_METHYLENE_L, "terminal_carbon_descriptor_n" ), // 'n': =C(H)(H)
	DZ2_METHYLENE_U( CarbonDescriptor.DZ2_METHYLENE_U, "terminal_carbon_descriptor_N" ), // 'N': =C(X)(X)
	DZ2_CISTRANS_EL( CarbonDescriptor.DZ2_CISTRANS_EL, "terminal_carbon_descriptor_e" ), // 'e': =C(X)(H)
	DZ2_CISTRANS_ZL( CarbonDescriptor.DZ2_CISTRANS_ZL, "terminal_carbon_descriptor_z" ), // 'z': =C(X)(H)
	DZ2_CISTRANS_XL( CarbonDescriptor.DZ2_CISTRANS_XL, "terminal_carbon_descriptor_f" ), // 'f': =C(X)(H)
	DZ2_CISTRANS_EU( CarbonDescriptor.DZ2_CISTRANS_EU, "terminal_carbon_descriptor_E" ), // 'E': =C(X)(Y)
	DZ2_CISTRANS_ZU( CarbonDescriptor.DZ2_CISTRANS_ZU, "terminal_carbon_descriptor_Z" ), // 'Z': =C(X)(Y)
	DZ2_CISTRANS_XU( CarbonDescriptor.DZ2_CISTRANS_XU, "terminal_carbon_descriptor_F" ), // 'F': =C(X)(Y)
	SZ1_XETHYNE    ( CarbonDescriptor.SZ1_XETHYNE    , "terminal_carbon_descriptor_T" ), // 'T': -C(#X)
	DZ1_KETENE_U   ( CarbonDescriptor.DZ1_KETENE_U   , "terminal_carbon_descriptor_K" ), // 'K': =C(=X)
	TZ1_ETHYNE_L   ( CarbonDescriptor.TZ1_ETHYNE_L   , "terminal_carbon_descriptor_t" ), // 't': #C(H)
	TZ1_ETHYNE_U   ( CarbonDescriptor.TZ1_ETHYNE_U   , "terminal_carbon_descriptor_T" ), // 'T': #C(X)
	// Anomeric
	SZX_ANOMER     ( CarbonDescriptor.SZX_ANOMER     , "carbon_descriptor_a" ), // 'a': -C(X)(Y)(H) at anomeric position
	// Ambiguous
	SZX_UNDEF_L    ( CarbonDescriptor.SZX_UNDEF_L    , "terminal_carbon_descriptor_u" ), // 'u': -C(X)(Y)(H) or -C(=O)(H)
	SZX_UNDEF_ALL  ( CarbonDescriptor.SZX_UNDEF_ALL  , "carbon_descriptor_Q" ), // 'Q': terminal carbon type undefined at all

	// Non-terminal
	SS3_METHYNE    ( CarbonDescriptor.SS3_METHYNE    , "non-terminal_carbon_descriptor_d" ), // 'd': -C(H)(H)-
	SS3_ACETAL     ( CarbonDescriptor.SS3_ACETAL     , "non-terminal_carbon_descriptor_C" ), // 'C': -C(X)(X)-
	SS3_STEREO_S_L ( CarbonDescriptor.SS3_STEREO_S_L , "non-terminal_carbon_descriptor_1" ), // '1': -C(X)(Y)-
	SS3_STEREO_R_L ( CarbonDescriptor.SS3_STEREO_R_L , "non-terminal_carbon_descriptor_2" ), // '2': -C(X)(Y)-
	SS3_STEREO_s_L ( CarbonDescriptor.SS3_STEREO_s_L , "non-terminal_carbon_descriptor_3" ), // '3': -C(X)(Y)-
	SS3_STEREO_r_L ( CarbonDescriptor.SS3_STEREO_r_L , "non-terminal_carbon_descriptor_4" ), // '4': -C(X)(Y)-
	SS3_STEREO_X_L ( CarbonDescriptor.SS3_STEREO_X_L , "non-terminal_carbon_descriptor_x" ), // 'x': -C(X)(Y)-
	SS3_STEREO_S_U ( CarbonDescriptor.SS3_CHIRAL_S_U , "non-terminal_carbon_descriptor_5" ), // '5': -C(X)(Y)-
	SS3_STEREO_R_U ( CarbonDescriptor.SS3_CHIRAL_R_U , "non-terminal_carbon_descriptor_6" ), // '6': -C(X)(Y)-
	SS3_STEREO_s_U ( CarbonDescriptor.SS3_CHIRAL_s_U , "non-terminal_carbon_descriptor_7" ), // '7': -C(X)(Y)-
	SS3_STEREO_r_U ( CarbonDescriptor.SS3_CHIRAL_r_U , "non-terminal_carbon_descriptor_8" ), // '8': -C(X)(Y)-
	SS3_STEREO_X_U ( CarbonDescriptor.SS3_CHIRAL_X_U , "non-terminal_carbon_descriptor_X" ), // 'X': -C(X)(Y)-
	SS2_KETONE_U   ( CarbonDescriptor.SS2_KETONE_U   , "non-terminal_carbon_descriptor_O" ), // 'O': -C(=X)-
	DS2_CISTRANS_EL( CarbonDescriptor.DS2_CISTRANS_EL, "non-terminal_carbon_descriptor_e" ), // 'e': =C(H)-
	DS2_CISTRANS_ZL( CarbonDescriptor.DS2_CISTRANS_ZL, "non-terminal_carbon_descriptor_z" ), // 'z': =C(H)-
	DS2_CISTRANS_NL( CarbonDescriptor.DS2_CISTRANS_NL, "non-terminal_carbon_descriptor_n" ), // 'n': =C(H)-
	DS2_CISTRANS_XL( CarbonDescriptor.DS2_CISTRANS_XL, "non-terminal_carbon_descriptor_f" ), // 'f': =C(H)-
	DS2_CISTRANS_EU( CarbonDescriptor.DS2_CISTRANS_EU, "non-terminal_carbon_descriptor_E" ), // 'E': =C(H)-
	DS2_CISTRANS_ZU( CarbonDescriptor.DS2_CISTRANS_ZU, "non-terminal_carbon_descriptor_Z" ), // 'Z': =C(H)-
	DS2_CISTRANS_NU( CarbonDescriptor.DS2_CISTRANS_NU, "non-terminal_carbon_descriptor_N" ), // 'N': =C(H)-
	DS2_CISTRANS_XU( CarbonDescriptor.DS2_CISTRANS_XU, "non-terminal_carbon_descriptor_F" ), // 'F': =C(H)-
	DD1_KETENE     ( CarbonDescriptor.DD1_KETENE     , "non-terminal_carbon_descriptor_K" ), // 'K': =C=
	TS1_ETHYNE     ( CarbonDescriptor.TS1_ETHYNE     , "non-terminal_carbon_descriptor_T" ), // 'T': #C-
	// Anomeric
	SSX_ANOMER     ( CarbonDescriptor.SSX_ANOMER     , "carbon_descriptor_a" ), // 'a': -C(X)(Y)- at anomeric position
	// Ambiguous
	SSX_UNDEF_U    ( CarbonDescriptor.SSX_UNDEF_U    , "non-terminal_carbon_descriptor_U" ), // 'U': 'a':-C(X)(Y)- or 'O'-C(=X)-
	SSX_UNDEF_ALL  ( CarbonDescriptor.SSX_UNDEF_ALL  , "carbon_descriptor_Q" ), // 'Q': non terminal carbon type undefined at all

	XXX_UNKNOWN    ( CarbonDescriptor.XXX_UNKNOWN    , "carbon_descriptor_unknown" ); // '?': C???

	private CarbonDescriptor m_enumCD;
	private String m_strURIFragment;

	private CarbonDescriptorURI(CarbonDescriptor a_enumCD, String a_strURIFragment) {
		this.m_enumCD = a_enumCD;
		this.m_strURIFragment = a_strURIFragment;
	}

	public CarbonDescriptor getCarbonDescriptor() {
		return this.m_enumCD;
	}

	public String getNS() {
		return WURCSOWL.NS;
	}

	public String getURIFragment() {
		return this.m_strURIFragment;
	}

	public String getURI() {
		return getNS()+getURIFragment();
	}

	public CarbonDescriptorURI forCharacter( char a_cCarbonDescriptor, Boolean a_bIsTerminal ) {
		CarbonDescriptor t_enumCD = CarbonDescriptor.forCharacter(a_cCarbonDescriptor, a_bIsTerminal );
		for ( CarbonDescriptorURI t_enumCDURI : CarbonDescriptorURI.values() ) {
			if ( t_enumCDURI.m_enumCD != t_enumCD ) continue;
			return t_enumCDURI;
		}
		return null;
	}
}
