package org.glycoinfo.WURCSFramework.wurcs.rdf;

import org.glycoinfo.WURCSFramework.wurcs.rdf.constant.GLYCAN_OLD;
import org.glycoinfo.WURCSFramework.wurcs.rdf.constant.WURCS_OLD;
import org.glycoinfo.WURCSFramework.wurcs.rdf.constant.WURCSSEQ;
import org.glycoinfo.WURCSFramework.wurcs.sequence2.GLIN;
import org.glycoinfo.WURCSFramework.wurcs.sequence2.GRES;
import org.glycoinfo.WURCSFramework.wurcs.sequence2.WURCSSequence2;


public class WURCSSequence2ExporterRDFModel extends WURCSRDFModel {

	@SuppressWarnings("unused")
	private WURCSSequence2ExporterRDFModel(){}

	public WURCSSequence2ExporterRDFModel(String a_strAccessionNumber, WURCSSequence2 a_oWURCS, boolean a_bDefault){
		super(a_bDefault);
		if ( a_bDefault ) {
			this.addPrefix(PrefixList.GLYCAN.getPrefix(), PrefixList.GLYCAN.getPrefixURI());
			this.addPrefix(PrefixList.WURCS.getPrefix(), PrefixList.WURCS.getPrefixURI());
		}
		start(a_strAccessionNumber, a_oWURCS);
	}

	private WURCSSequence2ExporterRDFModel start(String a_strAccessionNumber, WURCSSequence2 a_oSeq) {

		WURCSSequence2ExporterRDFModel t_oModel = this;

		String t_strGSEQURL = WURCSTripleURL.GSEQ_AC.get(a_strAccessionNumber, null);

		//// Saccharide triple
		t_oModel.addTriple(createResourceTriple(
				WURCSTripleURL.GLYCAN_AC.get(a_strAccessionNumber, null),
				GLYCAN_OLD.HAS_GSEQ,
				t_strGSEQURL));


		//// Glycosequence triple

		// For GRES
		for ( GRES t_oGRES : a_oSeq.getGRESs() ) {

			String t_strGRESURL = WURCSTripleURL.GRES_AC.get(a_strAccessionNumber, t_oGRES.getID());

			t_oModel.addTriple(createResourceTriple(
				t_strGSEQURL, WURCSSEQ.HAS_GRES, t_strGRESURL
			));

			if ( t_oGRES.getID() != 1 ) continue;

			//For root
			t_oModel.addTriple(createResourceTriple(
				t_strGSEQURL, WURCSSEQ.HAS_ROOT_GRES, t_strGRESURL
			));
		}

		// For WURCS sequence
		t_oModel.addTriple(createLiteralTriple(
			t_strGSEQURL, GLYCAN_OLD.HAS_SEQ, a_oSeq.getWURCS()
		));

		// predicate is RES_count
		t_oModel.addTriple(createLiteralTriple(
				t_strGSEQURL,
				WURCS_OLD.NUM_RES,
				a_oSeq.getGRESCount()));
		// predicate is LIN_count
		t_oModel.addTriple(createLiteralTriple(
				t_strGSEQURL,
				WURCS_OLD.NUM_LIN,
				a_oSeq.getGLINCount()));
		// predicate is uniqueRES_Count
		t_oModel.addTriple(createLiteralTriple(
				t_strGSEQURL,
				WURCS_OLD.NUM_URES,
				a_oSeq.getMSCount()));

		// predicate is count_of_RES
		t_oModel.addTriple(createLiteralTriple(
				t_strGSEQURL,
				WURCS_OLD.COUNT_RES,
				a_oSeq.getGRESCount()));
		// predicate is count_of_LIN
		t_oModel.addTriple(createLiteralTriple(
				t_strGSEQURL,
				WURCS_OLD.COUNT_LIN,
				a_oSeq.getGLINCount()));
		// predicate is count_of_uniqueRES
		t_oModel.addTriple(createLiteralTriple(
				t_strGSEQURL,
				WURCS_OLD.COUNT_URES,
				a_oSeq.getMSCount()));		
		
		

		// GRES triple
		for ( GRES t_oGRES : a_oSeq.getGRESs() ) {

			String t_strGRESURL = WURCSTripleURL.GRES_AC.get(a_strAccessionNumber, t_oGRES.getID());

			t_oModel.addTriple(createResourceTriple(
				t_strGRESURL, WURCSSEQ.IS_MS, WURCSTripleURL.MS.get(a_strAccessionNumber, t_oGRES.getMS().getString() )
			));

			// For GLIN of donor side
			for ( GLIN t_oGLIN : t_oGRES.getDonorGLINs() ) {
				t_oModel.addTriple(createResourceTriple(
					t_strGRESURL, WURCSSEQ.IS_DONOR, WURCSTripleURL.GLIN_AC.get(a_strAccessionNumber, t_oGLIN.getID() )
				));
			}
			// For GLIN of acceptor side
			for ( GLIN t_oGLIN : t_oGRES.getAcceptorGLINs() ) {
				t_oModel.addTriple(createResourceTriple(
					t_strGRESURL, WURCSSEQ.IS_ACCEPTOR, WURCSTripleURL.GLIN_AC.get(a_strAccessionNumber, t_oGLIN.getID() )
				));
			}
		}

		// GLIN triple
		for ( GLIN t_oGLIN : a_oSeq.getGLINs() ) {

			String t_strGLINURL = WURCSTripleURL.GLIN_AC.get(a_strAccessionNumber, t_oGLIN.getID());

			// For donor monosaccharide
			for ( GRES t_oGRES : t_oGLIN.getDonor() ) {
				t_oModel.addTriple(createResourceTriple(
					t_strGLINURL, WURCSSEQ.HAS_DONOR, WURCSTripleURL.GRES_AC.get(a_strAccessionNumber, t_oGRES.getID() )
				));
			}

			// For donor position
			for ( Integer t_iPos : t_oGLIN.getDonorPositions() ) {
				t_oModel.addTriple(createLiteralTriple(
					t_strGLINURL, WURCSSEQ.HAS_D_POS, t_iPos
				));

				// For unknown position
				if ( t_iPos != -1 ) continue;
				for ( GRES t_oGRES : t_oGLIN.getDonor() ) {
					for ( Integer t_iPossPos : t_oGRES.getMS().getPossiblePositions() ) {
						t_oModel.addTriple(createLiteralTriple(
								t_strGLINURL, WURCSSEQ.HAS_D_POS, t_iPossPos
							));
					}
				}
			}
			// For acceptor monosaccharide
			for ( GRES t_oGRES : t_oGLIN.getAcceptor() ) {
				t_oModel.addTriple(createResourceTriple(
					t_strGLINURL, WURCSSEQ.HAS_ACCEPTOR, WURCSTripleURL.GRES_AC.get(a_strAccessionNumber, t_oGRES.getID() )
				));
			}
			// For acceptor position
			for ( Integer t_iPos : t_oGLIN.getAcceptorPositions() ) {
				t_oModel.addTriple(createLiteralTriple(
					t_strGLINURL, WURCSSEQ.HAS_A_POS, t_iPos
				));

				// For unknown position
				if ( t_iPos != -1 ) continue;
				for ( GRES t_oGRES : t_oGLIN.getAcceptor() ) {
					for ( Integer t_iPossPos : t_oGRES.getMS().getPossiblePositions() ) {
						t_oModel.addTriple(createLiteralTriple(
								t_strGLINURL, WURCSSEQ.HAS_A_POS, t_iPossPos
							));
					}
				}
			}

			// For MAP
			t_oModel.addTriple(createLiteralTriple(
					t_strGLINURL, WURCSSEQ.HAS_MAP, t_oGLIN.getMAP()
			));

		}

		return t_oModel;
	}

}
