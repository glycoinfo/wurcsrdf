package org.glycoinfo.WURCSFramework.wurcs.rdf;

import java.util.LinkedList;

import org.glycoinfo.WURCSFramework.util.array.WURCSExporter;
import org.glycoinfo.WURCSFramework.util.array.WURCSMonosaccharideIntegratorOld;
import org.glycoinfo.WURCSFramework.wurcs.array.GLIP;
import org.glycoinfo.WURCSFramework.wurcs.array.GLIPs;
import org.glycoinfo.WURCSFramework.wurcs.array.LIN;
import org.glycoinfo.WURCSFramework.wurcs.array.RES;
import org.glycoinfo.WURCSFramework.wurcs.array.UniqueRES;
import org.glycoinfo.WURCSFramework.wurcs.array.WURCSArray;
import org.glycoinfo.WURCSFramework.wurcs.rdf.constant.GLYCAN_OLD;
import org.glycoinfo.WURCSFramework.wurcs.rdf.constant.WURCS_OLD;
import org.openrdf.model.vocabulary.OWL;
import org.openrdf.model.vocabulary.RDF;

public class WURCSRDFModelGlycan052 extends WURCSRDFModel{

	@SuppressWarnings("unused")
	private WURCSRDFModelGlycan052(){}

	public WURCSRDFModelGlycan052(String a_strAccessionNumber, WURCSArray a_oWURCS){
		super();
		createWURCSGlycanTripleModel(a_strAccessionNumber, a_oWURCS);
	}

	private WURCSRDFModelGlycan052 createWURCSGlycanTripleModel(String a_strAccessionNumber, WURCSArray a_oWURCS){

		WURCSExporter export = new WURCSExporter();

		// # WURCS
		String t_strWURCSString = export.getWURCSString(a_oWURCS);

		WURCSRDFModelGlycan052 model = this;

		// Saccharide triple
		model.addTriple(createResourceTriple(
				WURCSTripleURLOld.GLYAN.get(a_strAccessionNumber, null),
				RDF.TYPE.stringValue(),
				GLYCAN_OLD.A_SACCHARIDE));

		model.addTriple(createResourceTriple(
				WURCSTripleURLOld.GLYAN.get(a_strAccessionNumber, null),
				GLYCAN_OLD.HAS_GSEQ,
				WURCSTripleURLOld.GLY_WURCS.get(a_strAccessionNumber, null)));

		// Glycosequence triple
		model.addTriple(createResourceTriple(
				WURCSTripleURLOld.GLY_WURCS.get(a_strAccessionNumber, null),
				RDF.TYPE.stringValue(),
				GLYCAN_OLD.A_GSEQ));

		// For uniqueRES count
		model.addTriple(createLiteralTriple(
				WURCSTripleURLOld.GLY_WURCS.get(a_strAccessionNumber, null),
				WURCS_OLD.NUM_URES,
				a_oWURCS.getUniqueRESCount()));

		// For RES count
		model.addTriple(createLiteralTriple(
				WURCSTripleURLOld.GLY_WURCS.get(a_strAccessionNumber, null),
				WURCS_OLD.NUM_RES,
				a_oWURCS.getRESCount()));

		// For LIN count
		model.addTriple(createLiteralTriple(
				WURCSTripleURLOld.GLY_WURCS.get(a_strAccessionNumber, null),
				WURCS_OLD.NUM_LIN,
				a_oWURCS.getLINCount()));

		// For root RES
		model.addTriple(createResourceTriple(
				WURCSTripleURLOld.GLY_WURCS.get(a_strAccessionNumber, null),
				WURCS_OLD.HAS_ROOT_RES,
				WURCSTripleURLOld.RES.get(a_strAccessionNumber, "a")));

		for (UniqueRES t_oURES : a_oWURCS.getUniqueRESs()) {
			// For unique RES ID
			model.addTriple(createResourceTriple(
					WURCSTripleURLOld.GLY_WURCS.get(a_strAccessionNumber, null),
					WURCS_OLD.HAS_URES,
					WURCSTripleURLOld.UNIQ_RES.get(a_strAccessionNumber, t_oURES.getUniqueRESID())));

			// For monosaccharide of unique RES
			model.addTriple(createResourceTriple(
					WURCSTripleURLOld.GLY_WURCS.get(a_strAccessionNumber, null),
					WURCS_OLD.HAS_MS,
					WURCSTripleURLOld.MS.get(a_strAccessionNumber, export.getUniqueRESString(t_oURES))));

			// For basetype of unique RES
			UniqueRES t_oBasetype = WURCSMonosaccharideIntegratorOld.convertBasetype(t_oURES);
			model.addTriple(createResourceTriple(
					WURCSTripleURLOld.GLY_WURCS.get(a_strAccessionNumber, null),
					WURCS_OLD.HAS_BASETYPE,
					WURCSTripleURLOld.BASE_TYPE.get(a_strAccessionNumber, export.getUniqueRESString(t_oBasetype))));
		}

		for ( LIN t_oLIN :a_oWURCS.getLINs() ) {
			// For LIN
			model.addTriple(createResourceTriple(
					WURCSTripleURLOld.GLY_WURCS.get(a_strAccessionNumber, null),
					WURCS_OLD.HAS_LIN,
					WURCSTripleURLOld.LIN.get(a_strAccessionNumber, export.getLINString(t_oLIN))));
		}

		// For WURCS sequence
		model.addTriple(createLiteralTriple(
				WURCSTripleURLOld.GLY_WURCS.get(a_strAccessionNumber, null),
				GLYCAN_OLD.HAS_SEQ,
				t_strWURCSString));

		// For format
		model.addTriple(createResourceTriple(
				WURCSTripleURLOld.GLY_WURCS.get(a_strAccessionNumber, null),
				GLYCAN_OLD.IN_CARB_FORMAT,
				GLYCAN_OLD.FORMAT_WURCS));

		// Same as
		model.addTriple(createResourceTriple(
				WURCSTripleURLOld.GLY_WURCS.get(a_strAccessionNumber, null),
				OWL.SAMEAS.stringValue(),
				WURCSTripleURLOld.WURCS_URL.get(a_strAccessionNumber,  export.getWURCSString(a_oWURCS) )));

		// UniqueRES triple
		for ( UniqueRES t_oURES : a_oWURCS.getUniqueRESs() ) {
			model.addTriple(createResourceTriple(
					WURCSTripleURLOld.UNIQ_RES.get(a_strAccessionNumber, t_oURES.getUniqueRESID()),
					RDF.TYPE.stringValue(),
					WURCS_OLD.A_URES));

			// For monosaccharide of unique RES
			model.addTriple(createResourceTriple(
					WURCSTripleURLOld.UNIQ_RES.get(a_strAccessionNumber, t_oURES.getUniqueRESID()),
					WURCS_OLD.IS_MS,
					WURCSTripleURLOld.MS.get(a_strAccessionNumber, export.getUniqueRESString(t_oURES))));
		}

		// RES triple
		for ( RES t_oRES : a_oWURCS.getRESs() ) {
			model.addTriple(createResourceTriple(
					WURCSTripleURLOld.RES.get(a_strAccessionNumber, t_oRES.getRESIndex()),
					RDF.TYPE.stringValue(),
					WURCS_OLD.A_RES));

			// For uniqueRES of this RES
			model.addTriple(createResourceTriple(
					WURCSTripleURLOld.RES.get(a_strAccessionNumber, t_oRES.getRESIndex()),
					WURCS_OLD.IS_URES,
					WURCSTripleURLOld.UNIQ_RES.get(a_strAccessionNumber, t_oRES.getUniqueRESID())));
			// For LIN contained this RES
			for ( LIN t_oLIN : a_oWURCS.getLINs() ) {
				if ( !t_oLIN.containRES(t_oRES) ) continue;
				model.addTriple(createResourceTriple(
						WURCSTripleURLOld.RES.get(a_strAccessionNumber, t_oRES.getRESIndex()),
						WURCS_OLD.HAS_LIN,
						WURCSTripleURLOld.LIN.get(a_strAccessionNumber, export.getLINString(t_oLIN))));
			}
		}

		// LIN triple
		LinkedList<GLIPs> t_aGLIPs = new LinkedList<GLIPs>();
		for ( LIN t_oLIN : a_oWURCS.getLINs() ) {
			model.addTriple(createResourceTriple(
					WURCSTripleURLOld.LIN.get(a_strAccessionNumber, export.getLINString(t_oLIN)),
					RDF.TYPE.stringValue(),
					WURCS_OLD.A_LIN));

			// For MAP
			if ( !t_oLIN.getMAPCode().isEmpty() ) {
				model.addTriple(createLiteralTriple(
						WURCSTripleURLOld.LIN.get(a_strAccessionNumber, export.getLINString(t_oLIN)),
						WURCS_OLD.HAS_MAP,
						t_oLIN.getMAPCode()));
			}

			// For repeating
			model.addTriple(createLiteralTriple(
					WURCSTripleURLOld.LIN.get(a_strAccessionNumber, export.getLINString(t_oLIN)),
					WURCS_OLD.IS_REP,
					t_oLIN.isRepeatingUnit()));
			if ( t_oLIN.isRepeatingUnit() ) {
				model.addTriple(createLiteralTriple(
						WURCSTripleURLOld.LIN.get(a_strAccessionNumber, export.getLINString(t_oLIN)),
						WURCS_OLD.HAS_REP_MIN,
						t_oLIN.getMinRepeatCount()));
				model.addTriple(createLiteralTriple(
						WURCSTripleURLOld.LIN.get(a_strAccessionNumber, export.getLINString(t_oLIN)),
						WURCS_OLD.HAS_REP_MAX,
						t_oLIN.getMaxRepeatCount()));
			}

			// For GLIPS
			for ( GLIPs t_oGLIPs : t_oLIN.getListOfGLIPs() ) {
				t_aGLIPs.add(t_oGLIPs);
				model.addTriple(createResourceTriple(
						WURCSTripleURLOld.LIN.get(a_strAccessionNumber, export.getLINString(t_oLIN)),
						WURCS_OLD.HAS_GLIPS,
						WURCSTripleURLOld.GLIPS.get(a_strAccessionNumber, export.getGLIPsString(t_oGLIPs))));
			}
		}

		// GLIPS triple
		LinkedList<GLIP> t_aGLIP = new LinkedList<GLIP>();
		for ( GLIPs t_oGLIPs : t_aGLIPs ) {
			model.addTriple(createResourceTriple(
					WURCSTripleURLOld.GLIPS.get(a_strAccessionNumber, export.getGLIPsString(t_oGLIPs)),
					RDF.TYPE.stringValue(),
					WURCS_OLD.A_GLIPS));

			// Is fuzzy
			model.addTriple(createLiteralTriple(
					WURCSTripleURLOld.GLIPS.get(a_strAccessionNumber, export.getGLIPsString(t_oGLIPs)),
					WURCS_OLD.IS_FUZZY,
					t_oGLIPs.isFuzzy()));

			for ( GLIP t_oGLIP : t_oGLIPs.getGLIPs() ) {
				t_aGLIP.add(t_oGLIP);
				model.addTriple(createResourceTriple(
						WURCSTripleURLOld.GLIPS.get(a_strAccessionNumber, export.getGLIPsString(t_oGLIPs)),
						WURCS_OLD.HAS_GLIP,
						WURCSTripleURLOld.GLIP.get(a_strAccessionNumber, export.getGLIPString(t_oGLIP))));
			}
		}

		// GLIP triple
		for ( GLIP t_oGLIP : t_aGLIP ) {
			model.addTriple(createResourceTriple(
					WURCSTripleURLOld.GLIP.get(a_strAccessionNumber, export.getGLIPString(t_oGLIP)),
					RDF.TYPE.stringValue(),
					WURCS_OLD.A_GLIP));

			// For RES index
			model.addTriple(createResourceTriple(
					WURCSTripleURLOld.GLIP.get(a_strAccessionNumber, export.getGLIPString(t_oGLIP)),
					WURCS_OLD.HAS_RES,
					WURCSTripleURLOld.RES.get(a_strAccessionNumber, t_oGLIP.getRESIndex())));

			// For probabilities
			if ( t_oGLIP.getBackboneProbabilityLower() != 1.0 ) {
				model.addTriple(createLiteralTriple(
						WURCSTripleURLOld.GLIP.get(a_strAccessionNumber, export.getGLIPString(t_oGLIP)),
						WURCS_OLD.HAS_B_PROB_LOW,
						t_oGLIP.getBackboneProbabilityLower()));
				model.addTriple(createLiteralTriple(
						WURCSTripleURLOld.GLIP.get(a_strAccessionNumber, export.getGLIPString(t_oGLIP)),
						WURCS_OLD.HAS_B_PROB_UP,
						t_oGLIP.getBackboneProbabilityUpper()));
			}
			if ( t_oGLIP.getModificationProbabilityLower() != 1.0 ) {
				model.addTriple(createLiteralTriple(
						WURCSTripleURLOld.GLIP.get(a_strAccessionNumber, export.getGLIPString(t_oGLIP)),
						WURCS_OLD.HAS_M_PROB_LOW,
						t_oGLIP.getModificationProbabilityLower()));
				model.addTriple(createLiteralTriple(
						WURCSTripleURLOld.GLIP.get(a_strAccessionNumber, export.getGLIPString(t_oGLIP)),
						WURCS_OLD.HAS_M_PROB_UP,
						t_oGLIP.getModificationProbabilityUpper()));
			}

			// For SC position
			model.addTriple(createLiteralTriple(
					WURCSTripleURLOld.GLIP.get(a_strAccessionNumber, export.getGLIPString(t_oGLIP)),
					WURCS_OLD.HAS_SC_POS,
					t_oGLIP.getBackbonePosition()));
			// For direction
			if ( t_oGLIP.getBackboneDirection() != ' ' ) {
				model.addTriple(createLiteralTriple(
						WURCSTripleURLOld.GLIP.get(a_strAccessionNumber, export.getGLIPString(t_oGLIP)),
						WURCS_OLD.HAS_DIRECTION,
						t_oGLIP.getBackboneDirection()));
			}
			// For MAP position (TODO: MAP position -> star index)
			if ( t_oGLIP.getModificationPosition() != 0 ) {
				model.addTriple(createLiteralTriple(
						WURCSTripleURLOld.GLIP.get(a_strAccessionNumber, export.getGLIPString(t_oGLIP)),
						WURCS_OLD.HAS_STAR_INDEX,
						t_oGLIP.getModificationPosition()));
			}
		}
		return model;
	}
}
