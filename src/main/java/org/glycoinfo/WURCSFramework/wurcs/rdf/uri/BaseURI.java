package org.glycoinfo.WURCSFramework.wurcs.rdf.uri;

/**
 * Class for base URI related GlycoInfo RDF
 * @author MasaakiMatsubara
 *
 */
public class BaseURI {

	// http://rdf.glycoinfo.org
	public static final String GLYCOINFO = "http://rdf.glycoinfo.org";
	// http://rdf.glycoinfo.org/glycan
	public static final String GLYCAN    = GLYCOINFO+"/glycan";

	// Component & alias
	// http://rdf.glycoinfo.org/component
	public static final String COMPONENT = GLYCOINFO+"/component";
	// http://rdf.glycoinfo.org/monosaccharide/alias
	public static final String MS_ALIAS  = GLYCOINFO+"/monosaccharide/alias";
	// http://rdf.glycoinfo.org/monosaccharide/alias/wurcs
	public static final String MS_ALIAS_WURCS    = MS_ALIAS+"/wurcs";
	// http://rdf.glycoinfo.org/monosaccharide/alias/carbbank
	public static final String MS_ALIAS_CARBBANK = MS_ALIAS+"/carbbank";
	// http://rdf.glycoinfo.org/monosaccharide/alias/iupac_condensed
	public static final String MS_ALIAS_IUPAC_CONDENS = MS_ALIAS+"/iupac_condensed";

	/** http://rdf.glycoinfo.org/glycan/{AccNum} */
	public static final String GLYCAN( String a_strAccNum ) { return GLYCAN+"/"+a_strAccNum; };
}
