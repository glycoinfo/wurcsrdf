package org.glycoinfo.WURCSFramework.wurcs.rdf.constant;

//import com.hp.hpl.jena.rdf.model.Property;
//import com.hp.hpl.jena.rdf.model.Resource;

public class GLYCAN_OLD{
	public static final String PREFIX = "glycan";
	public static final String NS = "http://purl.jp/bio/12/glyco/glycan#";
	public static String getPrefix() {return PREFIX;}
	public static String getURI() {return NS;}
	public static String getURI(String val) {return NS+val;}
//	public static final Resource NAMESPACE = m_model.createResource( NS );
//	public static final Resource FULL_LANG = m_model.getResource( getURI() );
	public static final String BASE_URL = "http://rdf.glycoinfo.org/glycan";

	// Property
	public static final String HAS_GSEQ			= getURI("has_glycosequence");
	public static final String HAS_SEQ			= getURI("has_sequence");
	public static final String IN_CARB_FORMAT	= getURI("in_carbohydrate_format");

	// Object
	public static final String A_SACCHARIDE		= getURI("Saccharide");
	public static final String A_GSEQ			= getURI("Glycosequence");
	public static final String FORMAT_WURCS		= getURI("carbohydrate_format_wurcs");

	// Component & Alias
	public static final String A_COMPONENT		= getURI("component");
	public static final String A_MS				= getURI("monosaccharide");
	public static final String MS_SCHEME_WURCS		= getURI("monosaccharide_notation_scheme_wurcs");
	public static final String MS_SCHEME_IUPAC_COND	= getURI("monosaccharide_notation_scheme_iupac_condensed");
	public static final String MS_SCHEME_CARBBANK	= getURI("monosaccharide_notation_scheme_carbbank");

	public static final String HAS_COMPONENT	= getURI("has_component");
	public static final String HAS_CARDINALITY	= getURI("has_cardinality");
	public static final String HAS_MS			= getURI("has_monosaccharide");
	public static final String HAS_ALIAS		= getURI("has_alias");
	public static final String HAS_ALIAS_NAME	= getURI("has_alias_name");
	public static final String HAS_MS_SCHME		= getURI("has_monosaccharide_notation_scheme");
}
