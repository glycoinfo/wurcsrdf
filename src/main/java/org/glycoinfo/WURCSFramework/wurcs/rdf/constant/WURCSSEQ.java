package org.glycoinfo.WURCSFramework.wurcs.rdf.constant;


public class WURCSSEQ extends WURCS_OLD {
	public static final String HAS_GRES			= getURI("has_GRES");
	public static final String HAS_ROOT_GRES	= getURI("has_root_GRES");
	// For residues
	public static final String IS_DONOR			= getURI("is_donor_of");
	public static final String IS_ACCEPTOR		= getURI("is_acceptor_of");
	// For linkages
	public static final String HAS_DONOR		= getURI("has_donor");
	public static final String HAS_ACCEPTOR		= getURI("has_acceptor");
	public static final String HAS_D_MS			= getURI("has_donor_MS");
	public static final String HAS_A_MS			= getURI("has_acceptor_MS");
	public static final String HAS_D_POS		= getURI("has_donor_position");
	public static final String HAS_A_POS		= getURI("has_acceptor_position");
	// For monosaccharides
	public static final String HAS_CORE			= getURI("has_core_structure");
	public static final String HAS_SUBST		= getURI("has_substituent");
	public static final String HAS_BRIDGE		= getURI("has_bridge");
	public static final String HAS_POS			= getURI("has_position_first");
	public static final String HAS_FIRST_POS	= getURI("has_position_first");
	public static final String HAS_SECOND_POS	= getURI("has_position_second");
}
