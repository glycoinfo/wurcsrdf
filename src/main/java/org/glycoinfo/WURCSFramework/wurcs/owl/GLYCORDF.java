package org.glycoinfo.WURCSFramework.wurcs.owl;

public class GLYCORDF {
	public static final String PREFIX = "glycan";
	public static final String NS = "http://purl.jp/bio/12/glyco/glycan#";
	public static String getHashURI(String a_strFragment) {return NS+a_strFragment;}

	// Object type
	public static final String A_SACCHARIDE		= NS+"Saccharide";
	public static final String A_GSEQ			= NS+"Glycosequence";

	// Individual
	public static final String FORMAT_WURCS		= NS+"carbohydrate_format_wurcs";

	// Property
	public static final String HAS_GSEQ			= NS+"has_glycosequence";
	public static final String HAS_SEQ			= NS+"has_sequence";
	public static final String IN_CARB_FORMAT	= NS+"in_carbohydrate_format";

	// Component & Alias
	public static final String A_COMPONENT		= NS+"component";
	public static final String A_MS				= NS+"monosaccharide";
	public static final String A_MS_ALIAS		= NS+"monosaccharide_alias";
	public static final String MS_SCHEME_WURCS		= NS+"monosaccharide_notation_scheme_wurcs";
	public static final String MS_SCHEME_IUPAC_COND	= NS+"monosaccharide_notation_scheme_iupac_condensed";
	public static final String MS_SCHEME_CARBBANK	= NS+"monosaccharide_notation_scheme_carbbank";

	public static final String HAS_COMPONENT	= NS+"has_component";
	public static final String HAS_CARDINALITY	= NS+"has_cardinality";
	public static final String HAS_MS			= NS+"has_monosaccharide";
	public static final String HAS_ALIAS		= NS+"has_alias";
	public static final String HAS_ALIAS_NAME	= NS+"has_alias_name";
	public static final String HAS_MS_SCHME		= NS+"has_monosaccharide_notation_scheme";

}
