package org.glycoinfo.WURCSFramework.wurcs.owl;

public class WURCSOWL {
	public static final String PREFIX = "wurcs";
	public static final String NS = "http://www.glycoinfo.org/glyco/owl/wurcs#";
	public static String getHashURI(String a_strFragment) {return NS+a_strFragment;}

	// Object type
	public static final String A_WURCS			= NS+"WURCS";	// SubclassOf GLYCORDF.A_GSEQ
	public static final String A_COUNT_SECT		= NS+"Count_section";
	public static final String A_URES_SECT		= NS+"UniqueRES_section";
	public static final String A_RESSEQ_SECT	= NS+"RES_sequence_section";
	public static final String A_LIN_SECT		= NS+"LIN_section";
	public static final String A_URES	= NS+"UniqueRES";
	public static final String A_RES	= NS+"RES";
	public static final String A_LIN	= NS+"LIN";
	public static final String A_GLIPS	= NS+"GLIPS";
	public static final String A_GLIP	= NS+"GLIP";
	public static final String A_MS		= NS+"Monosaccharide";
	public static final String A_MOD	= NS+"MOD";
	public static final String A_LIPS	= NS+"LIPS";
	public static final String A_LIP	= NS+"LIP";
	public static final String A_MAP	= NS+"MAP";

	public static final String A_URES_NUM	= NS+"UniqueRES_number";
	public static final String A_RES_ID		= NS+"RES_index";
	public static final String A_C_NUM		= NS+"Carbon_number";
	public static final String A_DIRECTION	= NS+"Direction";
	public static final String A_MAP_NUM	= NS+"MAP_star_number";

	public static final String A_SS			= NS+"SkeletonStructure";
	public static final String A_ANOMER		= NS+"Anomer";
	public static final String A_SC			= NS+"SkeletonCode";
	public static final String A_CARBON		= NS+"SkeletonCarbon";
	public static final String A_CD			= NS+"CarbonDescriptor";

	public static final String A_PROB		= NS+"Probability";
	public static final String A_B_PROB		= NS+"Backbone_probability";		// Subclass of A_PROB
	public static final String A_M_PROB		= NS+"Modification_probability";	// Subclass of A_PROB
	public static final String A_PROB_VAL	= NS+"Probability_value";
	public static final String A_REP		= NS+"Repeat";
	public static final String A_REP_COUNT	= NS+"Repeat_count";

	// Property
	public static final String HAS_WURCS		= NS+"has_WURCS";
	public static final String HAS_URES_COUNT	= NS+"has_UniqueRES_count";
	public static final String HAS_RES_COUNT	= NS+"has_RES_count";
	public static final String HAS_LIN_COUNT	= NS+"has_LIN_count";
	public static final String IS_MS			= NS+"is_monosaccharide";
	public static final String HAS_SC			= NS+"has_SkeletonCode";
	public static final String HAS_CARBON		= NS+"has_carbon";
	public static final String HAS_C_POS		= NS+"has_carbon_position";
	public static final String HAS_ANOM_POS		= NS+"has_anomeric_position";
	public static final String HAS_ANOM_SYMBOL	= NS+"has_anomeric_symbol";
	public static final String HAS_DIRECTION	= NS+"has_direction";
	public static final String HAS_STAR_NUM		= NS+"has_MAP_star_number";
	public static final String HAS_MOD			= NS+"has_MOD";
	public static final String HAS_MAP			= NS+"has_MAP";
	public static final String HAS_LIPS			= NS+"has_LIPS";
	public static final String HAS_LIP			= NS+"has_LIP";
	public static final String HAS_LIN			= NS+"has_LIN";
	public static final String HAS_GLIPS		= NS+"has_GLIPS";
	public static final String HAS_GLIP			= NS+"has_GLIP";
	public static final String HAS_RES			= NS+"has_RES";
	public static final String HAS_URES			= NS+"has_UniqueRES";
	public static final String HAS_REP			= NS+"has_repeat";
	public static final String HAS_REP_MAX		= NS+"has_repeat_count_max";
	public static final String HAS_REP_MIN		= NS+"has_repeat_count_min";
	public static final String HAS_B_PROB		= NS+"has_backbone_probability";
	public static final String HAS_M_PROB		= NS+"has_modification_probability";
	public static final String HAS_PROB_LOW		= NS+"has_probability_lower";
	public static final String HAS_PROB_UP		= NS+"has_probability_upper";
	public static final String SUBSUMES			= NS+"subsumes";

	// Property for MS
}
