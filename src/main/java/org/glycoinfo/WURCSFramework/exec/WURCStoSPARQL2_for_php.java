package org.glycoinfo.WURCSFramework.exec;

import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;
import org.glycoinfo.WURCSFramework.util.array.WURCSImporter;
import org.glycoinfo.WURCSFramework.util.exchange.WURCSArrayToSequence2;
import org.glycoinfo.WURCSFramework.util.rdf.WURCSSequence2ExporterSPARQL;
import org.glycoinfo.WURCSFramework.wurcs.array.WURCSArray;
import org.glycoinfo.WURCSFramework.wurcs.sequence2.WURCSSequence2;

public class WURCStoSPARQL2_for_php {

	public static void main(String[] args) {

		String t_strWURCS = "";
		t_strWURCS = "WURCS=2.0/5,9,8/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][a2112h-1a_1-5]/1-1-2-3-1-3-1-4-5/a4-b1_b4-c1_c3-d1_c6-f1_d2-e1_f2-g1_g?-h1_h?-i1";
		// GlyTouCan G00012MO
		t_strWURCS = "WURCS=2.0/4,4,3/[u2122h][a2112h-1b_1-5][a2112h-1a_1-5][a2112h-1b_1-5_2*NCC/3=O]/1-2-3-4/a4-b1_b3-c1_c3-d1";
		
		WURCSSequence2ExporterSPARQL t_oExport = new WURCSSequence2ExporterSPARQL();

		for (int i=0; i<args.length; ++i) {
			if ( !args[i].startsWith("-") ) {
				t_strWURCS = args[i];
				break;
			}

			// For target graph
			if ("-tg".equals(args[i])) {
				String t_strGraph = args[++i];
				if ( t_strGraph.startsWith("<") && t_strGraph.endsWith(">") )
					t_oExport.addTargetGraphURI(t_strGraph);
				continue;
			}
			// For monosaccharide graph
			if ("-mg".equals(args[i])) {
				String t_strGraph = args[++i];
				if ( t_strGraph.startsWith("<") && t_strGraph.endsWith(">") )
					t_oExport.setMSGraphURI(t_strGraph);
				continue;
			}

			// For options
			if ( args[i].contains("c") )	// Chenge result to count
				t_oExport.setCountOption(true);
			if ( args[i].contains("s") )	// Superstructure search for monosaccharide
				t_oExport.setSearchSupersumption(true);
			if ( args[i].contains("i") )	// Isomer search (not search positions)
				t_oExport.setSearchIsomer(true);
			if ( args[i].contains("h") )	// Hide comments
				t_oExport.hideComments(true);
			if ( args[i].contains("g") )	// Ignore Self
				t_oExport.setIgnoreSelf(true);
			
						
		}

		try {
			WURCSImporter t_oImport = new WURCSImporter();
			WURCSArray t_oWURCS = t_oImport.extractWURCSArray(t_strWURCS);
			WURCSArrayToSequence2 t_oA2S = new WURCSArrayToSequence2();
			t_oA2S.start(t_oWURCS);
			WURCSSequence2 t_oSeq = t_oA2S.getSequence();

			t_oExport.start(t_oSeq);
			String t_strSPARQL = t_oExport.getQuery();

			System.out.println(t_strSPARQL);

		} catch (WURCSFormatException e) {
			e.printStackTrace();
		}
	}
}
