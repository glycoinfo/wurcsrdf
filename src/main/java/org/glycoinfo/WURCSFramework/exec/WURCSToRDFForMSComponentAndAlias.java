package org.glycoinfo.WURCSFramework.exec;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TreeMap;

import org.glycoinfo.WURCSFramework.util.FileIOUtils;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.util.WURCSFileWriter;
import org.glycoinfo.WURCSFramework.util.WURCSListReader;
import org.glycoinfo.WURCSFramework.wurcs.array.WURCSArray;
import org.glycoinfo.WURCSFramework.wurcs.rdf.WURCSRDFModel;
import org.glycoinfo.WURCSFramework.wurcs.rdf.WURCSRDFModelMSComponentAndAlias;

public class WURCSToRDFForMSComponentAndAlias {
	private static String m_strMSAliasRDF = "MS-Alias-RDF-0.1.ttl";


	public static void main(String[] args) throws Exception {

		String t_strRDFPath  = "E:\\RDF\\GlyTouCan\\";

		String t_strWURCSListFile = t_strRDFPath + "20160127result-GlyTouCan_WURCSList.txt";
		File file = new File(t_strWURCSListFile);

		if(!file.isFile()) throw new Exception();


		// Start
		Date day = new Date();
		long start = day.getTime();

		// Output prefix
		Boolean t_bPrefix = true;

		// Read file
		TreeMap<String, String> t_mapIndexToWURCS = WURCSListReader.readFromFile(t_strWURCSListFile);

		try{
			// Open print writer
			PrintWriter pwRDF = FileIOUtils.openTextFileW( WURCSFileWriter.getResultFilePath(t_strRDFPath, m_strMSAliasRDF) );

			// Add prefix
			WURCSRDFModel t_oRDFPrefix = new WURCSRDFModel();
			pwRDF.println( t_oRDFPrefix.get_RDFPrefix("TURTLE") );

			// For all WURCS
			for(String key : t_mapIndexToWURCS.keySet()) {
				System.out.println(key);
				String t_strAccessionNumber = key;

				WURCSFactory t_oFactory = new WURCSFactory(t_mapIndexToWURCS.get(key));
				WURCSArray t_oArray = t_oFactory.getArray();

				// Generate RDF strings (MS component and alias)
				WURCSRDFModelMSComponentAndAlias t_oExport = new WURCSRDFModelMSComponentAndAlias( t_strAccessionNumber, t_oArray, t_bPrefix );
				pwRDF.println( t_oExport.getRDFWithoutPrefix("TURTLE") );

				// For log
				PrintWriter pwLog = FileIOUtils.openTextFileW( WURCSFileWriter.getResultFilePath(t_strRDFPath, "RDFConversionResult.log") );
				pwLog.println("Date:\n"+(new SimpleDateFormat("yyyy/MM/dd-HH:mm:ss")).format(new Date())+"\n");
				pwLog.println("Read from:\n"+t_strWURCSListFile+"\n");
				pwLog.println("Output files:");
				String t_strDate = (new SimpleDateFormat("yyyyMMdd")).format(new Date());
				pwLog.println(t_strDate+m_strMSAliasRDF);
				pwLog.close();
			}
			pwRDF.close();

		} catch (IOException e) {
			System.out.println(e);
		}

		// Termination
		System.out.println("\nFin...");
		day = new Date();
		long end = day.getTime();
		System.out.println("time:" + (end - start) + " m sec.");
	}
}
