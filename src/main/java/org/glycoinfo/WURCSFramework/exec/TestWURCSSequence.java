package org.glycoinfo.WURCSFramework.exec;

import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;
import org.glycoinfo.WURCSFramework.util.array.WURCSImporter;
import org.glycoinfo.WURCSFramework.wurcs.rdf.WURCSSequence2ExporterRDFModel;
import org.glycoinfo.WURCSFramework.wurcs.sequence2.WURCSSequence2;

public class TestWURCSSequence {

	public static void main(String[] args) {

		String input;
		// GlycomeDB: 4385
		input = "WURCS=2.0/2,4,4/[22122h-1a_1-5][h5122h-2b_2-5]/1-1-2-2/a1-b1_b6-c2_c1-d2_c1-c2~n";
		input = "WURCS=2.0/13,14,13/[aUd21122h][211221h-1a_1-5][211221h-1a_1-5_4*OPO/3O/3=O][22122a-1a_1-5][22122h-1a_1-5_2*N][12122h-1b_1-5][12112m-1b_1-5_2*NCC/3=O_4*N][11122a-1b_1-5_2*NCC/3=O_3*NCC/3=O][22112h-1a_1-5_2*NCC/3=O][12122A-1b_1-5_2*NCC/3=O_3*NCC/3=O_6*=O_6*N][11122A-1b_1-5_2*NCC/3=O_3*NCC/3=O_6*=O_6*N][112eEH-1b_1-5_2*OCC/3=O_3*OCC/3=O_6*N][22112a-1a_1-5_2*N]/1-2-3-4-5-6-5-7-8-9-10-11-12-13/a5-b1_b3-c1_b4-f1_c2-d1_c7-e1_f4-g1_f6-n1_g6-h1_h3-i1_i4-j1_j4-k1_k4-l1_l4-m1";
		input = "WURCS=2.0/2,6,7/[22122h-1a_1-5][22112h-1a_1-5]/1-1-1-1-1-2/a1-c4_a4-b1_b4-c1_c6-d1_d4-e1_e6-f1_b1-b4~n";
		input = "WURCS=2.0/5,5,5/[h222h][12112h-1b_1-5_2*NCC/3=O_6*OPO/3O/3=O][22112h-1a_1-5_2*NCC/3=O_6*OPO/3O/3=O][22112m-1a_1-5_2*NCC/3=O_4*N][12122h-1b_1-5]/1-2-3-4-5/a5-b1_b3-c1_c4-d1_d3-e1_a1-e6*OP^XO*/3O/3=O~n";
	//	input = "WURCS=2.0/2,4,5/[12122h-1b_1-5][hxh]/1-1-1-2/a1-c2_a2-b1_b2-c1_c6-d1*OP^XO*/3O/3=O_b1-b2~n";
		input = "WURCS=2.0/7,12,14/[x2122h-1x_1-5_2*NCC/3=O][12122h-1b_1-5_2*NCC/3=O][11122h-1b_1-5][21122h-1a_1-5][xxxxxh-1x_1-?_2*NCC/3=O][12112h-1b_1-5][a6d21122h-2a_2-6_5*NCC/3=O]/1-2-3-4-5-6-7-7-4-5-6-7/a4-b1_b4-c1_e4-f1_f3-g2_g8-h2_j4-k1_k3-l2_c?-d1_c?-i1_d?-e1_i?-j1_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?}*OCC/3=O_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?}*OCC/3=O_a?|b?|c?|d?|e?|f?|g?|h?|i?|j?|k?|l?}*OCC/3=O";
		input = "WURCS=2.0/6,10,9/[h2122h_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][Aad21122h-2a_2-6_5*NCC/3=O]/1-2-3-4-2-5-6-4-4-4/a4-b1_b4-c1_c3-d1_c6-h1_d2-e1_e4-f1_f6-g2_h3-i1_h6-j1";

		WURCSImporter t_objImporter = new WURCSImporter();
		try {
			// Import WURCS without error messages
			WURCSFactory t_oFactory = new WURCSFactory(input);
			WURCSSequence2 t_oSeq = t_oFactory.getSequence();

			WURCSSequence2ExporterRDFModel t_oExporter = new WURCSSequence2ExporterRDFModel("GXXXXXXX", t_oSeq, true);
			System.out.println( t_oExporter.get_RDF("TURTLE") );


		} catch (WURCSFormatException e) {
			System.err.println( e.getErrorMessage() );
			e.printStackTrace();
		} catch (WURCSException e) {
			System.err.println( e.getErrorMessage() );
			e.printStackTrace();
		}

	}

}
