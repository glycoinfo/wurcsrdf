package org.glycoinfo.WURCSFramework.exec;

import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;
import org.glycoinfo.WURCSFramework.util.array.WURCSImporter;
import org.glycoinfo.WURCSFramework.util.exchange.WURCSArrayToSequence2;
import org.glycoinfo.WURCSFramework.util.rdf.WURCSSequence2ExporterSPARQL;
import org.glycoinfo.WURCSFramework.wurcs.array.WURCSArray;
import org.glycoinfo.WURCSFramework.wurcs.sequence2.WURCSSequence2;

public class WURCStoSPARQL2 {
	public static void main(String[] args) {

		String input = "";
		// GlyTouCan: G00031MO
		input = "WURCS=2.0/2,2,1/[a2112h-1a_1-5_2*NCC/3=O][a2112h-1b_1-5]/1-2/a3-b1";
		// GlyTouCan: G00030MO
		input = "WURCS=2.0/4,7,6/[u2122h_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-2-3-4-2-4-2/a4-b1_b4-c1_c3-d1_c6-f1_e1-d2|d4_g1-f2|f4";
		// GlyTouCan: G80382WR
//		input = "WURCS=2.0/5,9,8/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][axxxxh-1b_1-?_2*NCC/3=O]/1-2-3-4-2-4-2-5-5/a4-b1_b4-c1_c3-d1_c6-f1_d2-e1_f2-g1_h1-a?|b?|c?|d?|e?|f?|g?}_i1-a?|b?|c?|d?|e?|f?|g?}";

		try {
			WURCSImporter t_oImport = new WURCSImporter();
			WURCSArray t_oWURCS = t_oImport.extractWURCSArray(input);
			WURCSArrayToSequence2 t_oA2S = new WURCSArrayToSequence2();
			t_oA2S.start(t_oWURCS);
			WURCSSequence2 t_oSeq = t_oA2S.getSequence();

			WURCSSequence2ExporterSPARQL t_oExport = new WURCSSequence2ExporterSPARQL();

			// Set option for SPARQL query generator
//			t_oExport.setCountOption(true); // True: Count result
			//t_oExport.addTargetGraphURI("<http://rdf.glycoinfo.org/wurcs/seq>"); // Add your target graph
			t_oExport.addTargetGraphURI("<http://rdf.glytoucan.org/sequence/wurcs>"); // Add your target graph
			t_oExport.addTargetGraphURI("<http://rdf.glytoucan.org/>");
			//t_oExport.setMSGraphURI("<http://rdf.glycoinfo.org/wurcs/0.5.1/ms>"); // Set your monosaccharide graph
			t_oExport.setMSGraphURI("<http://rdf.glytoucan.org/wurcs/ms>"); // Set your monosaccharide graph
//			t_oExport.hideComments(true); // Hide all comments in query
			//t_oExport.setSearchSupersumption(true); // Search supersumption of monosaccharide
			t_oExport.setSearchIsomer(true); // Search isomer
			t_oExport.setIgnoreSelf(true); // Search without itself
//			t_oExport.setSpecifyRootNode(true); // Specify root node

			t_oExport.start(t_oSeq);
			String t_strSPARQL = t_oExport.getQuery();

			System.out.println(t_strSPARQL);

		} catch (WURCSFormatException e) {
			e.printStackTrace();
		}

	}

}
