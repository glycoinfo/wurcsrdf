package org.glycoinfo.WURCSFramework.exec;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.TreeMap;
import java.util.TreeSet;

import org.glycoinfo.WURCSFramework.util.FileIOUtils;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.util.WURCSFileWriter;
import org.glycoinfo.WURCSFramework.util.WURCSListReader;
import org.glycoinfo.WURCSFramework.util.array.WURCSExporter;
import org.glycoinfo.WURCSFramework.util.array.WURCSImporter;
import org.glycoinfo.WURCSFramework.wurcs.array.MS;
import org.glycoinfo.WURCSFramework.wurcs.array.UniqueRES;
import org.glycoinfo.WURCSFramework.wurcs.array.WURCSArray;
import org.glycoinfo.WURCSFramework.wurcs.rdf.WURCSRDFModel;
import org.glycoinfo.WURCSFramework.wurcs.rdf.WURCSRDFModelGlycan051;
import org.glycoinfo.WURCSFramework.wurcs.rdf.WURCSRDFModelGlycan052;
import org.glycoinfo.WURCSFramework.wurcs.rdf.WURCSRDFModelMS;
import org.glycoinfo.WURCSFramework.wurcs.rdf.WURCSSequence2ExporterRDFModel;
import org.glycoinfo.WURCSFramework.wurcs.sequence2.WURCSSequence2;
import org.glycoinfo.WURCSFramework.util.SHA2;
import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.array.*;
import org.glycoinfo.WURCSFramework.wurcs.*;
import org.glycoinfo.WURCSFramework.wurcs.array.MS;
import org.glycoinfo.WURCSFramework.util.array.WURCSImporter;
import org.glycoinfo.WURCSFramework.util.array.*;
public class WURCStoRDF_Mac {

	private static String m_strRDF1    = "WURCS-RDF-0.5.1.ttl";
	private static String m_strRDF2    = "WURCS-RDF-0.5.2.ttl";
	private static String m_strSEQRDF2 = "WURCS-SEQ-RDF-0.3.ttl";
	private static String m_strMSRDF2  = "WURCS-MS-RDF-0.2.ttl";


	public static void main(String[] args) throws Exception {

		String t_strRDFPath  = "/Users/yamada/testdata/";
		
		for(int i=0;i<args.length;i++){
	        t_strRDFPath = args[i];
		}

//		String t_strWURCSListFile = t_strRDFPath + "20150422glytoucan.wurcs.tsv";
//		String t_strWURCSListFile = t_strRDFPath + "20150728result-GlyTouCan_WURCSList.txt";
		//String t_strWURCSListFile = t_strRDFPath + "20150930result-GlyTouCan_WURCSList.txt";

		String t_strWURCSListFile = t_strRDFPath; // + "2018-12-13_GlyTouCan-WURCS_tax9606.tsv";
		//String t_strWURCSListFile = t_strRDFPath + "2018-12-13_GlyTouCan-WURCS_tax10090.tsv";
		File file = new File(t_strWURCSListFile);

		if(!file.isFile()) throw new Exception();


		// Start
		Date day = new Date();
		long start = day.getTime();

		// Output prefix
		Boolean t_bPrefix = true;

		// Read file
		TreeMap<String, String> t_mapIndexToWURCS = WURCSListReader.readFromFile(t_strWURCSListFile);

		try{
			// Open print writer
			PrintWriter pwRDF1 = FileIOUtils.openTextFileW( WURCSFileWriter.getResultFilePath(t_strRDFPath, m_strRDF1) );
			PrintWriter pwRDF2 = FileIOUtils.openTextFileW( WURCSFileWriter.getResultFilePath(t_strRDFPath, m_strRDF2) );
			PrintWriter pwSEQRDF2 = FileIOUtils.openTextFileW( WURCSFileWriter.getResultFilePath(t_strRDFPath, m_strSEQRDF2) );

			// Add prefix
			WURCSRDFModel t_oRDFPrefix = new WURCSRDFModel();
			pwRDF1.println( t_oRDFPrefix.get_RDFPrefix("TURTLE") );
			pwRDF2.println( t_oRDFPrefix.get_RDFPrefix("TURTLE") );
			pwSEQRDF2.println( t_oRDFPrefix.get_RDFPrefix("TURTLE") );

			// For all WURCS
			TreeSet<String> t_setUniqueMSs = new TreeSet<String>();
			for(String key : t_mapIndexToWURCS.keySet()) {
				WURCSFactory t_oFactory = new WURCSFactory(t_mapIndexToWURCS.get(key));

				// Collect unique MSs
				WURCSExporter t_oExport = new WURCSExporter();
				WURCSArray t_oArray = t_oFactory.getArray();
				for ( UniqueRES t_oURES : t_oArray.getUniqueRESs() ) {
					String t_strMS = t_oExport.getUniqueRESString(t_oURES);
					if ( t_setUniqueMSs.contains(t_strMS) ) continue;
					t_setUniqueMSs.add(t_strMS);
				}

				System.out.println(key);
				//String t_strAccessionNumber = key;
				// WURCS string
				String t_strWURCShash = SHA2.getSHA256(t_mapIndexToWURCS.get(key));

				// Generate RDF strings (ver 0.5.1)
				WURCSRDFModelGlycan051 t_oRDFExport2 = new WURCSRDFModelGlycan051( t_strWURCShash, t_oArray, t_bPrefix );
				pwRDF1.println( t_oRDFExport2.getRDFWithoutPrefix("TURTLE") );

				// Generate RDF strings (ver 0.5.2)
				WURCSRDFModelGlycan052 t_oRDFExport1 = new WURCSRDFModelGlycan052( t_strWURCShash, t_oArray );
				pwRDF2.println( t_oRDFExport1.getRDFWithoutPrefix("TURTLE") );

				// For using WURCSSequence
				WURCSSequence2 t_oSeq2 = t_oFactory.getSequence();

				WURCSSequence2ExporterRDFModel t_oSeq2Export = new WURCSSequence2ExporterRDFModel( t_strWURCShash, t_oSeq2, t_bPrefix );
				pwSEQRDF2.println( t_oSeq2Export.getRDFWithoutPrefix("TURTLE") );
			}
			pwRDF1.close();
			pwRDF2.close();
			pwSEQRDF2.close();

			// For unique MS
			System.out.println("\nExport MS...");
			WURCSImporter t_oImport = new WURCSImporter();
			LinkedList<MS> t_aUniqueMSs = new LinkedList<MS>();
			for ( String t_strMS : t_setUniqueMSs )
				t_aUniqueMSs.add( t_oImport.extractMS(t_strMS) );

			System.out.println("Total MS:"+t_setUniqueMSs.size());
			PrintWriter pwMSRDF2 = FileIOUtils.openTextFileW( WURCSFileWriter.getResultFilePath(t_strRDFPath, m_strMSRDF2) );
//			PrintWriter pwMSSUBSUMRDF2 = FileIOUtils.openTextFileW( WURCSFileWriter.getResultFilePath(t_strRDFPath, "WURCS-MS-SUBSUMPTION-RDF-0.2.ttl") );

			WURCSRDFModelMS t_oMSExport = new WURCSRDFModelMS(t_aUniqueMSs, t_bPrefix);
			pwMSRDF2.println( t_oMSExport.get_RDF("TURTLE") );

			pwMSRDF2.close();


			// For log
			PrintWriter pwLog = FileIOUtils.openTextFileW( WURCSFileWriter.getResultFilePath(t_strRDFPath, "RDFConversionResult.log") );
			pwLog.println("Date:\n"+(new SimpleDateFormat("yyyy/MM/dd-HH:mm:ss")).format(new Date())+"\n");
			pwLog.println("Read from:\n"+t_strWURCSListFile+"\n");
			pwLog.println("Output files:");
			String t_strDate = (new SimpleDateFormat("yyyyMMdd")).format(new Date());
			pwLog.println(t_strDate+m_strRDF1);
			pwLog.println(t_strDate+m_strRDF2);
			pwLog.println(t_strDate+m_strSEQRDF2);
			pwLog.println(t_strDate+m_strMSRDF2);
			pwLog.close();

		}catch(IOException e){
			System.out.println(e);
		}

		// Termination
		System.out.println("\nFin...");
		day = new Date();
		long end = day.getTime();
		System.out.println("time:" + (end - start) + " m sec.");
	}

}
