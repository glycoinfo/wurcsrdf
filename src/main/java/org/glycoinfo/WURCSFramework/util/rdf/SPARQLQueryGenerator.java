package org.glycoinfo.WURCSFramework.util.rdf;

import java.net.URL;
import java.util.LinkedList;

/**
 * Abstract class for creation SPARQL query
 * @author MasaakiMatsubara
 *
 */
public abstract class SPARQLQueryGenerator {

	private LinkedList<String> m_aTergetGraphURI = new LinkedList<String>();

	public void addTargetGraphURI(String a_strTergetGraphURI) {
		if ( this.m_aTergetGraphURI.contains(a_strTergetGraphURI) ) return;
		this.m_aTergetGraphURI.addLast(a_strTergetGraphURI);
	}

	public LinkedList<String> getTargetGraphURIs() {
		return this.m_aTergetGraphURI;
	}

	protected String getSELECTSection(LinkedList<String> a_aVar, boolean a_bDistinct) {
		String t_strSelect = "SELECT ";
		if ( a_bDistinct ) t_strSelect += "DISTINCT ";
		for ( String t_strVar : a_aVar ) {
			t_strSelect += t_strVar+" ";
		}
		t_strSelect += "\n";
		return t_strSelect;
	}

	protected String getCONSTRUCTSection(String a_strMainQuery) {
		String t_strConstruct = "CONSTRUCT {\n";
		t_strConstruct += a_strMainQuery+"\n";
		t_strConstruct += "}\n";
		return t_strConstruct;
	}

	protected String getFROMSection() {
		String t_strFrom = "";
		for ( String t_strGraph : this.m_aTergetGraphURI ) {
			t_strFrom += "FROM "+t_strGraph+"\n";
		}
		return t_strFrom;
	}

	protected String getFROMNAMEDSection(LinkedList<String> a_aTergetGraphURI) {
		String t_strFrom = "";
		for ( String t_strGraph : a_aTergetGraphURI ) {
			t_strFrom += "FROM NAMED "+t_strGraph+"\n";
		}
		return t_strFrom;
	}

	protected String getWHERESection(String a_strMainQuery) {
		String t_strWHERE = "WHERE {\n";
		t_strWHERE += a_strMainQuery;
		t_strWHERE += "}\n";
		return t_strWHERE;
	}

	protected String getORDERBY(LinkedList<String> a_aVars) {
		String t_strOrderBy = "";
		for ( String t_strVar : a_aVars ) {
			if ( !t_strOrderBy.equals("") ) t_strOrderBy += " ";
			t_strOrderBy += t_strVar;
		}
		t_strOrderBy = "ORDER BY "+t_strOrderBy+"\n";
		return t_strOrderBy;
	}

	protected String getGSPO(String a_strGraph, String a_strSubject, String a_strPredicate, String a_strObject) {
		String t_strGSPO = "  GRAPH "+a_strGraph+" {\n";
		t_strGSPO += "  "+this.getSPO(a_strSubject, a_strPredicate, a_strObject);
		t_strGSPO += "  }\n";
		return t_strGSPO;
	}

	protected String getSPO(String a_strSubject, String a_strPredicate, String a_strObject) {
		String t_strSPO = "  "+a_strSubject+" "+a_strPredicate+" "+a_strObject+" .\n";
		return t_strSPO;
	}

	protected String getLiteralWithType(Object obj){
		String t_strLiteral = "";
		String t_strType = "";
		if(obj instanceof  String){
			t_strLiteral = (String)obj;
			t_strType = "xsd:string";
		}else if( obj instanceof Integer){
			t_strLiteral = String.valueOf((Integer)obj);
			t_strType = "xsd:integer";
		}else if( obj instanceof Double){
			t_strLiteral = String.valueOf((Double)obj);
			t_strType = "xsd:double";
		}else if( obj instanceof Boolean){
			t_strLiteral = String.valueOf((Boolean)obj);
			t_strType = "xsd:boolean";
		}else if( obj instanceof Character){
			t_strLiteral = String.valueOf((Character)obj);
			t_strType = "xsd:string";
		}else if( obj instanceof URL){
			t_strLiteral = ((URL)obj).toString();
		}
		return "\""+t_strLiteral+"\"^^"+t_strType;
	}

	abstract public String getQuery();
}
