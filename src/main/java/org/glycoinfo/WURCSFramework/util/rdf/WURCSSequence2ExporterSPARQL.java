package org.glycoinfo.WURCSFramework.util.rdf;

import java.util.LinkedList;

import org.glycoinfo.WURCSFramework.wurcs.rdf.PredicateList;
import org.glycoinfo.WURCSFramework.wurcs.rdf.WURCSTripleURL;
import org.glycoinfo.WURCSFramework.wurcs.sequence2.GLIN;
import org.glycoinfo.WURCSFramework.wurcs.sequence2.GRES;
import org.glycoinfo.WURCSFramework.wurcs.sequence2.SEQMS;
import org.glycoinfo.WURCSFramework.wurcs.sequence2.WURCSSequence2;

/**
 * Class for creation of WURCSDequence SPARQL query
 * @author MasaakiMatsubara
 *
 */
public class WURCSSequence2ExporterSPARQL extends SPARQLQueryGenerator {

	private LinkedList<SEQMS>   m_aMS   = new LinkedList<SEQMS>();
	private LinkedList<GRES> m_aGRES = new LinkedList<GRES>();
	private LinkedList<GLIN> m_aGLIN = new LinkedList<GLIN>();
	private LinkedList<String> m_aAcceptorGRESsToVar = new LinkedList<String>();

	private String m_strGraphURI    = "<http://rdf.glytoucan.org/sequence/wurcs>";
//	private String m_strGraphURIPos = "<http://rdf.glycoinfo.org/wurcs/seq/0.3/pos>";
	private String m_strMSGraphURIDefault = "<http://rdf.glytoucan.org/wurcs/ms>";

	private String m_strMSGraphURI  = this.m_strMSGraphURIDefault;

	private boolean m_bIsCount     = false;
	private boolean m_bSearchSuper = false;
	private boolean m_bSearchIsomer = false;
	private boolean m_bSearchFuzzyToFuzzy = false;
	private boolean m_bSpecifyRootNode = false;
	private boolean m_bIgnoreSelf = false;

	private boolean m_bHideComments = false;

	private String m_strQuery = "";
	private String m_strMainQuery = "";
	private String m_strRESCountVar = "?rescount";
	private String m_strURESCountVar = "?urescount";
	private String m_strLINCountVar = "?lincount";
	private String m_strGlycanVar = "?glycan";
	private String m_strGlycanCountVar = "?gcount";

	/**
	 * Set flag for counting the number of glycans
	 * @param a_bCount
	 */
	public void setCountOption(boolean a_bCount) {
		this.m_bIsCount = a_bCount;
	}

	/**
	 * Set flag for searching supersumption relations of monosaccharide
	 * @param a_bSuper
	 */
	public void setSearchSupersumption(boolean a_bSuper) {
		this.m_bSearchSuper = a_bSuper;
	}

	/**
	 * Set flag for searching isomer
	 * @param a_bIsomer
	 */
	public void setSearchIsomer(boolean a_bIsomer) {
		this.m_bSearchIsomer = a_bIsomer;
	}

	/**
	 * Set flag for specifying root node
	 * @param a_bSetRoot
	 */
	public void setSpecifyRootNode(boolean a_bSetRoot) {
		this.m_bSpecifyRootNode = a_bSetRoot;
	}

	/**
	 * Set flag for ignoring self in search
	 * @param a_bIgnoreSelf
	 */
	public void setIgnoreSelf(boolean a_bIgnoreSelf) {
		this.m_bIgnoreSelf = a_bIgnoreSelf;
	}

	/**
	 * Set flag for hiding comments
	 * @param a_bHideComments
	 */
	public void hideComments(boolean a_bHideComments) {
		this.m_bHideComments = a_bHideComments;
	}

	/**
	 * Set graph of monosaccharides
	 * @param a_strMSGraphURI
	 */
	public void setMSGraphURI(String a_strMSGraphURI) {
		this.m_strMSGraphURI = a_strMSGraphURI;
	}

	/**
	 * Get SPARQL query
	 */
	public String getQuery() {
		return this.m_strQuery;
	}

	/**
	 * Get SPARQL query in the WHERE clause
	 */
	public String getWhere() {
		return this.m_strMainQuery;
	}

	/**
	 * Get Order By String
	 */
	public String getOrderByString(){
		LinkedList<String> t_aVars = new LinkedList<String>();
		t_aVars.add(this.m_strRESCountVar);
		t_aVars.add(this.m_strURESCountVar);
		t_aVars.add(this.m_strLINCountVar);
		t_aVars.add(this.m_strGlycanVar);
		t_aVars.add("desc (" + this.m_strGlycanCountVar + ")");

		return this.getORDERBY(t_aVars);
	}


	public void start(WURCSSequence2 a_oSeq) {
		this.clear();

		String t_strQuery = "";

		// Header and prefix
		if ( !this.m_bHideComments )
			t_strQuery += this.getHeaderString(a_oSeq.getWURCS());

		// SELECT
		LinkedList<String> t_aSELECTVar = new LinkedList<String>();
		boolean t_bDistinct = false;
		if ( this.m_bIsCount ) {
			t_aSELECTVar.add("(COUNT (DISTINCT " + this.m_strGlycanVar + ") AS ?count)");
		} else {
			t_aSELECTVar.add(this.m_strGlycanVar);
			t_aSELECTVar.add("(COUNT (" + this.m_strGlycanVar + ") AS " + this.m_strGlycanCountVar + ")"); // Count bound number of each distinct glycan
			t_aSELECTVar.add("(STR (?wurcs) AS ?WURCS)");
			t_bDistinct = true;
		}
		t_strQuery += this.getSELECTSection(t_aSELECTVar, t_bDistinct);

		if ( this.getTargetGraphURIs().isEmpty() ) {
			this.addTargetGraphURI(this.m_strGraphURI);
//			this.addTargetGraphURI(this.m_strGraphURIPos);
		}
		LinkedList<String> t_aNamedGraphURI = new LinkedList<String>();
		t_aNamedGraphURI.add(this.m_strMSGraphURI);

		// FROM
		t_strQuery += this.getFROMSection();
		// FROM NAMED
		t_strQuery += this.getFROMNAMEDSection(t_aNamedGraphURI);

		// WHERE
		t_strQuery += this.getWHERESection( this.getMainQuery(a_oSeq) );

		if ( !this.m_bIsCount ) {
			t_strQuery += this.getOrderByString();
		}

		this.m_strQuery = t_strQuery;
	}

	public String getMainQuery(WURCSSequence2 a_oSeq) {
		String t_strMain = this.getSPO(this.m_strGlycanVar, "glycan:has_glycosequence", "?gseq");
		t_strMain += this.getSPO("?gseq", PredicateList.COUNT_RES.getPredicateWithPrefix(), this.m_strRESCountVar);
		t_strMain += this.getSPO("?gseq", PredicateList.COUNT_URES.getPredicateWithPrefix(), this.m_strURESCountVar);
		t_strMain += this.getSPO("?gseq", PredicateList.COUNT_LIN.getPredicateWithPrefix(), this.m_strLINCountVar);
		t_strMain += this.getSPO("?gseq", "glycan:has_sequence", "?wurcs");

		if ( this.m_bIgnoreSelf )
			t_strMain += "  FILTER ( ?wurcs != \""+a_oSeq.getWURCS()+"\"^^xsd:string )\n";

		// TODO: not using regex for isomer
		if ( this.m_bSearchIsomer ) {
//			t_strMain += "  FILTER( regex(?wurcs, \"WURCS=2.0/"+a_oSeq.getMSCount()+","+a_oSeq.getGRESCount()+","+a_oSeq.getGLINCount()+"/\") )\n";
			t_strMain += "  FILTER ( ";
			t_strMain += this.m_strURESCountVar + " = \"" + a_oSeq.getMSCount()   + "\"^^xsd:integer && ";
			t_strMain += this.m_strRESCountVar  + " = \"" + a_oSeq.getGRESCount() + "\"^^xsd:integer && ";
			t_strMain += this.m_strLINCountVar  + " = \"" + a_oSeq.getGLINCount() + "\"^^xsd:integer )\n";
		}

		// For each donor GLIN of each GRES
		LinkedList<GLIN> t_aCheckedGLIN = new LinkedList<GLIN>();
		for ( GRES t_oGRES : a_oSeq.getGRESs() ) {
			for ( GLIN t_oGLIN : t_oGRES.getDonorGLINs() ) {
				if ( t_aCheckedGLIN.contains(t_oGLIN) ) continue;

				t_strMain += this.getGLINQuery(t_oGLIN);

				t_aCheckedGLIN.add(t_oGLIN);
			}
			if ( t_oGRES.getAcceptorGLINs().isEmpty() && t_oGRES.getDonorGLINs().isEmpty() )
				t_strMain += this.getGRESQuery(t_oGRES);
		}

		// Add designation of MAP for GLINs
		if (!this.m_bHideComments)
			t_strMain += "\n  # MAP\n\n";
		for ( GLIN t_oGLIN : t_aCheckedGLIN ) {
			String t_strGLINVar = "?GLIN"+t_oGLIN.getID();
			// Match MAP
			String t_strMAP = this.getLiteralWithType(t_oGLIN.getMAP());
//			t_strGLIN += this.getSPO(t_strGLINVar, "wurcs:has_MAP", t_strMAP);
			t_strMain += this.getSPO(t_strGLINVar, "wurcs:has_MAP", t_strMAP);
/*
			// Using filter
			t_strGLIN += this.getSPO(t_strGLINVar, "wurcs:has_MAP", "?MAP");
			t_strGLIN += "FILTER ( str(?MAP) = \""+a_oGLIN.getMAP()+"\" )\n";
*/
		}

		this.m_strMainQuery = t_strMain;
		return t_strMain;
	}

	private String getHeaderString(String a_strQueryStructure) {
		String t_strHeader = "";
		t_strHeader += "# ******************************************************\n";
		t_strHeader += "# SPARQL Generator for Glycan Search:\n";
		t_strHeader += "# Last update: 2015/12/14\n";
		t_strHeader += "# Query Structure:\n";
		t_strHeader += "# "+a_strQueryStructure+"\n";
		t_strHeader += "# ******************************************************\n";
		t_strHeader += "\n";
		t_strHeader += "DEFINE sql:select-option \"order\"\n";
		t_strHeader += "PREFIX glycan: <http://purl.jp/bio/12/glyco/glycan#>\n";
		t_strHeader += "PREFIX wurcs: <http://www.glycoinfo.org/glyco/owl/wurcs#>\n";

		return t_strHeader;
	}

	private String getGLINQuery(GLIN a_oGLIN) {
		this.m_aGLIN.addLast(a_oGLIN);

		String t_strGLIN = "";
		// GLIN variable string
		String t_strGLINVar = "?GLIN"+a_oGLIN.getID();
		if (!this.m_bHideComments)
			t_strGLIN += "\n  # GLIN"+a_oGLIN.getID()+"\n";

		// For acceptor GRES
		String t_strGRESa = "";
		boolean t_bIsFuzzy = false;
		if ( a_oGLIN.getAcceptor().size() > 1 ) {
			t_bIsFuzzy = true;

			String t_strAcceptorGRESs = "";
			for ( GRES t_oGRES : a_oGLIN.getAcceptor() ) {
				t_strGLIN += this.getGRESQuery(t_oGRES);
//				t_strGLIN += this.getSPO("?GRES"+t_iAGRESID, "wurcs:is_acceptor_of", t_strGLINVar);
				int t_iAGRESID = this.m_aGRES.indexOf(t_oGRES)+1;

				// Use IN
				if ( t_strAcceptorGRESs != "" ) t_strAcceptorGRESs += ", ";
				t_strAcceptorGRESs += "?GRES"+t_iAGRESID;
			}
			String t_strGRESaVar    = "?GRES"+a_oGLIN.getID()+"a";
			String t_strGRESaFilter = "  FILTER ( "+t_strGRESaVar+" IN ( "+t_strAcceptorGRESs+" ) )\n";
			if ( this.m_bSearchFuzzyToFuzzy ) {
				if ( this.m_aAcceptorGRESsToVar.contains(t_strAcceptorGRESs) ) {
					t_strGRESaVar = "?GRESa"+(this.m_aAcceptorGRESsToVar.indexOf(t_strAcceptorGRESs)+1);
					t_strGRESaFilter = "";
				} else {
					this.m_aAcceptorGRESsToVar.addLast(t_strAcceptorGRESs);
					t_strGRESaVar = "?GRESa"+(this.m_aAcceptorGRESsToVar.indexOf(t_strAcceptorGRESs)+1);
					t_strGRESaFilter = "  FILTER ( "+t_strGRESaVar+" IN ( "+t_strAcceptorGRESs+" ) )\n";
				}
			}
			t_strGRESa += this.getSPO(t_strGRESaVar, "wurcs:is_acceptor_of", t_strGLINVar);
			t_strGRESa += t_strGRESaFilter;
/*
				// Use = and ||
				if ( t_strAcceptorGRESs != "" ) t_strAcceptorGRESs += " || ";
				t_strAcceptorGRESs += t_strGRESaVar+" = ?GRES"+t_iAGRESID;
			}
			t_strGRESa += "  FILTER ( "+t_strAcceptorGRESs+" )\n";
*/
		} else {
			for ( GRES t_oGRES : a_oGLIN.getAcceptor() ) {
				t_strGRESa += this.getGRESQuery(t_oGRES);
				int t_iAGRESID = this.m_aGRES.indexOf(t_oGRES)+1;
				t_strGRESa += this.getSPO("?GRES"+t_iAGRESID, "wurcs:is_acceptor_of", t_strGLINVar);
			}
		}

		// For donor GRES
		String t_strGRESd = "";
		for ( GRES t_oGRES : a_oGLIN.getDonor() ) {
			t_strGRESd += this.getGRESQuery(t_oGRES);
			int t_iDGRESID = this.m_aGRES.indexOf(t_oGRES)+1;
			t_strGRESd += this.getSPO("?GRES"+t_iDGRESID, "wurcs:is_donor_of", t_strGLINVar);
		}

		// For positions
		String t_strPos = "";
		if ( !this.m_bSearchIsomer )
			t_strPos += this.getGLINPositionQuery(a_oGLIN, t_strGLINVar);

		// fuzzy side comes last
		if ( a_oGLIN.getAcceptor().size() > a_oGLIN.getDonor().size() ) {
			t_strGLIN += t_strGRESd+"\n"+t_strPos+t_strGRESa+"\n";
		} else {
			t_strGLIN += t_strGRESa+"\n"+t_strPos+t_strGRESd+"\n";
		}

		if ( this.m_bSearchFuzzyToFuzzy && t_bIsFuzzy ) {
			// GLIN Filter
			String t_strGLINFilter = "";
			for ( GLIN t_oGLIN : this.m_aGLIN ) {
				if ( t_oGLIN.equals(a_oGLIN) ) continue;

				if ( !t_strGLINFilter.equals("") ) t_strGLINFilter += " && ";
				t_strGLINFilter += t_strGLINVar+" != ?GLIN"+t_oGLIN.getID();
			}
			if ( !t_strGLINFilter.equals("") );
				t_strGLIN += "  FILTER ( "+t_strGLINFilter+" )\n\n";
		}

		return t_strGLIN;
	}

	private String getGLINPositionQuery(GLIN a_oGLIN, String a_strGLINVar) {
		String t_strGLINPos = "";

		if ( !a_oGLIN.getDonorPositions().isEmpty() ) {
			if ( a_oGLIN.getDonorPositions().size() == 1 ) {
				String t_strDPos = ""+a_oGLIN.getDonorPositions().getFirst();
				if ( !t_strDPos.equals("-1") )
					t_strGLINPos += this.getSPO(a_strGLINVar, "wurcs:has_donor_position", t_strDPos);
			} else {
				String t_strPosVar = a_strGLINVar+"PosD";
				t_strGLINPos += this.getSPO(a_strGLINVar, "wurcs:has_donor_position", t_strPosVar);
				t_strGLINPos += this.getPositionVALUES( t_strPosVar, a_oGLIN.getDonorPositions());
//				t_strGLINPos += this.getPositionIN( t_strPosVar, a_oGLIN.getDonorPositions());
//				t_strGLINPos += this.getPositionOR( t_strPosVar, a_oGLIN.getDonorPositions());
			}
		}

		if ( !a_oGLIN.getAcceptorPositions().isEmpty() ) {
			if ( a_oGLIN.getAcceptorPositions().size() == 1 ) {
				String t_strAPos = ""+a_oGLIN.getAcceptorPositions().getFirst();
				if ( !t_strAPos.equals("-1") )
					t_strGLINPos += this.getSPO(a_strGLINVar, "wurcs:has_acceptor_position", t_strAPos);
			} else {
				String t_strPosVar = a_strGLINVar+"PosA";
				t_strGLINPos += this.getSPO(a_strGLINVar, "wurcs:has_acceptor_position", t_strPosVar);
				t_strGLINPos += this.getPositionVALUES( t_strPosVar, a_oGLIN.getAcceptorPositions());
//				t_strGLINPos += this.getPositionIN( t_strPosVar, a_oGLIN.getAcceptorPositions());
//				t_strGLINPos += this.getPositionOR( t_strPosVar, a_oGLIN.getAcceptorPositions());
			}
		}

		return t_strGLINPos;
	}

	private String getPositionVALUES(String a_strVar, LinkedList<Integer> a_aPositions) {
		String t_strPosition = "";
		t_strPosition += "  VALUES ( "+a_strVar+" ) { ";
		for ( int t_iPos : a_aPositions ) {
			t_strPosition += "(\""+t_iPos+"\"^^xsd:integer"+") ";
		}
		t_strPosition += "}\n";
		return t_strPosition;
	}

	private String getPositionIN(String a_strVar, LinkedList<Integer> a_aPositions) {
		String t_strPosition = "";
		for ( int t_iPos : a_aPositions ) {
			if ( t_strPosition != "" ) t_strPosition += ", ";
			t_strPosition += "\""+t_iPos+"\"^^xsd:integer";
		}
		t_strPosition = "  FILTER ( "+a_strVar+" IN ( "+t_strPosition+" ) )\n";
		return t_strPosition;
	}

	private String getPositionOR(String a_strVar, LinkedList<Integer> a_aPositions) {
		String t_strPosition = "";
		for ( int t_iPos : a_aPositions ) {
			if ( t_strPosition != "" ) t_strPosition += " || ";
			t_strPosition += a_strVar+" = \""+t_iPos+"\"^^xsd:integer";
		}
		t_strPosition = "  FILTER ( "+t_strPosition+" )\n";
		return t_strPosition;
	}

	private String getMSQuery(SEQMS a_oMS) {
		if ( this.m_aMS.contains(a_oMS) ) return "";
		this.m_aMS.addLast(a_oMS);

		String t_strMSURI = WURCSTripleURL.MS.get("", a_oMS.getString());
		String t_strMSVar = "?MS"+this.m_aMS.size();
		String t_strPredicate = "wurcs:subsumes";
		if ( this.m_bSearchSuper )
			t_strPredicate += "|^wurcs:subsumes";
		String t_strMS = this.getGSPO(this.m_strMSGraphURI, "<"+t_strMSURI+">", t_strPredicate, t_strMSVar);
/*
		// Search supersumption using FILTER
		if ( this.m_bSearchSuper ) {
			String t_strMSVarSub = t_strMSVar+"Sub";
			String t_strMSVarSuper = t_strMSVar+"Super";
			t_strMS = "  FILTER ( "+t_strMSVar+" = "+t_strMSVarSub+" || "+t_strMSVar+" = "+t_strMSVarSuper+" )\n";
			t_strMS += this.getGSPO(this.m_strMSGraphURI, "<"+t_strMSURI+">", "wurcs:subsumes", t_strMSVarSub);
			t_strMS += this.getGSPO(this.m_strMSGraphURI, "<"+t_strMSURI+">", "^wurcs:subsumes", t_strMSVarSuper);
		}
/*
		// Search supersumption using UNION, more slower than FILTER
		if ( this.m_bSearchSuper ) {
			t_strMS = "{\n";
			t_strMS += this.getGSPO(this.m_strMSGraphURI, "<"+t_strMSURI+">", "wurcs:subsumes", t_strMSVar);
			t_strMS += "} UNION {\n";
			t_strMS += this.getGSPO(this.m_strMSGraphURI, "<"+t_strMSURI+">", "^wurcs:subsumes", t_strMSVar);
			t_strMS += "}\n";
		}
*/
		return t_strMS;
	}

	private String getGRESQuery(GRES a_oGRES) {
		if ( a_oGRES == null ) return "";
		if ( this.m_aGRES.contains(a_oGRES) ) return "";
		this.m_aGRES.addLast(a_oGRES);
		int t_iGRESID = this.m_aGRES.indexOf(a_oGRES)+1;
		String t_strGRES = "\n";
		if (!this.m_bHideComments)
			t_strGRES += "  ## GRES"+t_iGRESID+"\n";
		// For gseq
		t_strGRES += this.getSPO("?gseq", "wurcs:has_GRES", "?GRES"+t_iGRESID);

		// For first MS
		if ( this.m_bSpecifyRootNode && t_iGRESID == 1)
			t_strGRES += this.getSPO("?gseq", "wurcs:has_root_GRES", "?GRES"+t_iGRESID);

		// For MS
		String t_strMS = this.getMSQuery(a_oGRES.getMS());
		int t_iMSID = this.m_aMS.indexOf(a_oGRES.getMS())+1;
		t_strGRES += this.getSPO("?GRES"+t_iGRESID, "wurcs:is_monosaccharide", "?MS"+t_iMSID);

		// Return if MS was already appeared
		if ( !t_strMS.equals("") ) t_strGRES += t_strMS;

		// Filter GRES

		// Use "NOT IN"
		String t_strGRESFilter = "";
/*		for ( GRES t_oGRES : this.m_aGRES ) {
			if ( t_oGRES.equals(a_oGRES) ) continue;
			if ( !t_oGRES.getMS().equals(a_oGRES.getMS()) ) continue;

			if ( !t_strGRESFilter.equals("") ) t_strGRESFilter += ", ";
			t_strGRESFilter += "?GRES"+(this.m_aGRES.indexOf(t_oGRES)+1);
		}
		t_strGRESFilter = "?GRES"+t_iGRESID+" NOT IN ( "+ t_strGRESFilter+" )";
*/
		// Use "!="
		for ( GRES t_oGRES : this.m_aGRES ) {
			if ( t_oGRES.equals(a_oGRES) ) continue;
//			if ( !t_oGRES.getMS().equals(a_oGRES.getMS()) ) continue;

			if ( !t_strGRESFilter.equals("") ) t_strGRESFilter += " && ";
			t_strGRESFilter += "?GRES"+t_iGRESID+" != ?GRES"+(this.m_aGRES.indexOf(t_oGRES)+1);
		}
		if ( t_strGRESFilter.equals("") ) return t_strGRES;

		t_strGRES += "  FILTER ( "+t_strGRESFilter+" )\n\n";

		return t_strGRES;
	}

	private void clear() {
		this.m_aGRES = new LinkedList<GRES>();
		this.m_aMS   = new LinkedList<SEQMS>();

		this.m_strQuery = "";
	}
}
