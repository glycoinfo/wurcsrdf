This project has been migrated from bitbucket[https://bitbucket.org/glycosw/wurcsrdf/src/master/] for management.

# release note

* version 1.2.6 (2022-03-10)

  * typo: glycan:saccharide, glycan:glycosequence -> glycan:Saccharide, glycan:Glycosequence

# Build

## WURCSFramework で　daily-snapshotを利用している側で、mavenを用いて更新を反映しようとしてもできない場合

* https://gitlab.com/glycoinfo/wurcsframework/-/issues/18

```
$ mvn dependency:purge-local-repository
```

## 問題ない場合

```
$ mvn clean package
$ mvn clean install
```


# dependency:tree

```
$ mvn dependency:tree
[INFO] Scanning for projects...
[INFO] 
[INFO] ---------------< org.glycoinfo.WURCSFramework:WURCSRDF >----------------
[INFO] Building WURCSRDF 1.0.1
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-dependency-plugin:2.8:tree (default-cli) @ WURCSRDF ---
[INFO] org.glycoinfo.WURCSFramework:WURCSRDF:jar:1.0.1
[INFO] +- org.apache.jena:apache-jena-libs:pom:2.13.0:compile
[INFO] |  +- org.apache.jena:jena-tdb:jar:1.1.2:compile
[INFO] |  |  +- org.apache.jena:jena-arq:jar:2.13.0:compile
[INFO] |  |  |  +- org.apache.httpcomponents:httpclient:jar:4.2.6:compile
[INFO] |  |  |  |  +- org.apache.httpcomponents:httpcore:jar:4.2.5:compile
[INFO] |  |  |  |  \- commons-codec:commons-codec:jar:1.6:compile
[INFO] |  |  |  +- com.github.jsonld-java:jsonld-java:jar:0.5.1:compile
[INFO] |  |  |  |  +- com.fasterxml.jackson.core:jackson-core:jar:2.3.3:compile
[INFO] |  |  |  |  \- com.fasterxml.jackson.core:jackson-databind:jar:2.3.3:compile
[INFO] |  |  |  |     \- com.fasterxml.jackson.core:jackson-annotations:jar:2.3.0:compile
[INFO] |  |  |  +- org.apache.httpcomponents:httpclient-cache:jar:4.2.6:compile
[INFO] |  |  |  +- org.apache.thrift:libthrift:jar:0.9.2:compile
[INFO] |  |  |  +- org.slf4j:jcl-over-slf4j:jar:1.7.6:compile
[INFO] |  |  |  +- org.apache.commons:commons-csv:jar:1.0:compile
[INFO] |  |  |  \- org.apache.commons:commons-lang3:jar:3.3.2:compile
[INFO] |  |  \- org.apache.jena:jena-core:jar:2.13.0:compile
[INFO] |  |     +- org.apache.jena:jena-iri:jar:1.1.2:compile
[INFO] |  |     \- xerces:xercesImpl:jar:2.11.0:compile
[INFO] |  |        \- xml-apis:xml-apis:jar:1.4.01:compile
[INFO] |  +- org.slf4j:slf4j-api:jar:1.7.6:compile
[INFO] |  +- org.slf4j:slf4j-log4j12:jar:1.7.6:compile
[INFO] |  \- log4j:log4j:jar:1.2.17:compile
[INFO] +- org.openrdf.sesame:sesame-model:jar:2.8.3:compile
[INFO] |  \- org.openrdf.sesame:sesame-util:jar:2.8.3:compile
[INFO] \- org.glycoinfo:wurcsframework:jar:1.0.1:compile
[INFO]    \- ch.qos.logback:logback-classic:jar:1.2.3:compile
[INFO]       \- ch.qos.logback:logback-core:jar:1.2.3:compile
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  1.208 s
[INFO] Finished at: 2020-07-30T15:08:43+09:00
[INFO] ------------------------------------------------------------------------
```






# Use Jenaの説明 v0.1 by fujita@aist 20150324#

関連ファイルは以下です。

既存のファイルは変更していませんので、他の処理に影響は無いかと思います。

ご意見などがあれば、メールやSkypeなどにて連絡だくさい。

wurcsRDF/

・WURCSRDFModel.java　（RDF作成用のクラス：汎用）

・WURCSRDFModelGlycan.java（WURCSRDFModelのサブクラス）：WURCSGlycanRDF用

・WURCSRDFModelMs.java（WURCSRDFModelのサブクラス）：WURCSMSRDF用

・WURCSTriple.java　（Tripleを登録するクラス）

・WURCSTripleURL.java　（URIを作成するためのenumユーティリティ）


wurcsRDF/constant

・GLYCAN.java　（定数宣言用）

・GLYTOUCAN.java　（定数宣言用）

・WURCS.java　（定数宣言用）


使用したJena(v2.12.1)

　jena-arq-2.12.1.jar jena-core-2.12.1.jar


WURCSRDFModel.javaにテスト用にmainがありますので、ある程度の使い方はそこを追えばわかると思います。

簡単に概要を説明しますと。

・WURCSRDFModel.java：RDFのTripleを保有するクラス（汎用）

・WURCSRDFModelGlycan.java：GlycanWURCS用のクラス

・WURCSRDFModelMs.java：MS用のクラス

以上のクラスのどれかを使い、それにaddTriple()、removeTriple()などを行ってTripleを操作します。get_RDF(フォーマット)で指定したフォーマットのRDFを文字列
として取得できます。

WURCSRDFModelGlycanやWURCSRDFModelMsはクラス生成時にWURCSのデータを渡します。その後に必要があれば、Tripleの追加や削除もできます。

GlycanやMSのRDF作成処理はハッカソン時の処理のままなので、そのごTripleの追加や変更があれば修正する必要があります。

TripleのURIを作成する場合は「WURCSTripleURL」を使うと容易に作成できます。

定数値は基本的に「constant」パッケージの下のクラスにあります。


・WURCSRDFModel

	public WURCSRDFModel()	：コンストラクタ（Prefixのデフォルト設定有）

	public WURCSRDFModel(boolean set_default)	：コンストラクタ（Prefixのデフォルト設定有無指定付）

	public void addTriple(WURCSTriple triple)	：Triple追加

	public void removeTriple(WURCSTriple triple)	：Triple削除

	public void addPrefix(String prefix, String prefix_url)	：Prefix追加

	public HashMap<String,String> getPrefix()	：Prefix一覧取得

	public String get_RDF(String format)	：RDF文字列取得

	public Model get_Model()	：現在のJenaのModelクラス取得

	public static Model createDefaultModel(boolean set_default)：Jenaの初期Modelクラス取得

	public static WURCSTriple createResourceTriple(String subject, String predicate, String url_str)	:Tripleクラス作成（objectがURL）

	public static WURCSTriple createLiteralTriple(String subject, String predicate, Object object)	：Tripleクラス作成（objectが定数値）


・WURCSRDFModelGlycan

	WURCSRDFModelGlycan(String a_strAccessionNumber, WURCSArray a_oWURCS)	：コンストラクタ

・WURCSRDFModelMs

	public WURCSRDFModelMs(LinkedList<UniqueRES> a_aUniqueRESs)	：コンストラクタ

・WURCSTriple

	メンバそれぞれのgetter,setter

・WURCSTripleURL	（valの文字列はURLエンコードされます）

	WURCS_URL()	.get(null, Object val)

	MS()		.get(null, Object val)

	BASE_TYPE()	.get(null, Object val)

	ANO_BASE()	.get(null, Object val)

	MOD()		.get(null, Object val)

	SC()		.get(null, Object val)

	GLYAN()		.get(String ac_no, null)

	GLY_WURCS()	.get(String ac_no, null)

	GLIP()		.get(null, Object val)

	GLIPS()		.get(null, Object val)

	LIN()		.get(null, Object val)

	RES()		.get(null, Object val)

	LIP()		.get(null, Object val)

	LIPS()		.get(null, Object val)

	UNIQ_RES()	.get(String ac_no, Object val)


基本的なコード例：

（１）WURCSクラスからTURTLE形式のRDFを作成

	org.glycoinfo.WURCSFramework.wurcs.WURCSArray wa = (new org.glycoinfo.WURCSFramework.util.WURCSImporter()).extractWURCSArray("WURCS=.....");

	WURCSRDFModelGlycan wcmodel = new WURCSRDFModelGlycan("G00123XX", wa);

	System.out.println(wcmodel.get_RDF("TURTLE"));

（２）WURCSRDFModelGlycanに

	<http://rdf.glycoinfo.org/glycan/G00123XX/wurcs/2.0>

		wurcs:has_monosaccharide

		<http://rdf.glycoinfo.org/glycan/wurcs/2.0/monosaccharide/12122h> .

を追加

	WURCSTriple　wcs_trpl = WURCSRDFModel.createResourceTriple(

		WURCSTripleURL.GLY_WURCS.get("G00123XX", null),

		RDF.TYPE.stringValue(),

		WURCSTripleURL.MS.get(null, "12122h"));

	wcmodel.addTriple(wcs_trpl);


# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

